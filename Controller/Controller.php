<?php

/**
	Declare Routes Collection
	Format: [ [path: string, call: function] ]
**/
$routes = [
	[	
		'path' => '', 
		'call' => 
		function()
		{
			Encoder::set(['status' => 200, 'message' => 'Welcome to IFHTI API!']);
		}
	],
	[
		'path' => '/account/.+', 
		'call' => 
		function($call)
		{
			Model::object('Account', ['call' => $call ]);
		}
	],
	[
		'path' => '/patient/.+', 
		'call' => 
		function($call)
		{
			Model::object('Patient', ['call' => $call ]);
		}
	],
	[
		'path' => '/branch/.+', 'call' => 
		function($call)
		{
			Model::object('Branch', ['call' => $call ]);
		}
	],
	[
		'path' => '/file/.+', 'call' => 
		function($call)
		{
			Model::object('File', ['call' => $call ]);
		}
	],
	[
		'path' => '/philhealth/.+', 'call' => 
		function($call)
		{
			Model::object('PhilHealth', ['call' => $call ]);
		}
	],
	[
		'path' => '/news/.+', 'call' => 
		function($call)
		{
			Model::object('News', ['call' => $call ]);
		}
	],
	[
		'path' => '/note/.+', 'call' => 
		function($call)
		{
			Model::object('Note', ['call' => $call ]);
		}
	],
	[
		'path' => '/upload/.+', 'call' =>
		function($call)
		{
			Model::library('Upload', ['call' => $call]);				
		}
	],
	[
		'path' => '/patient-service/.+', 'call' =>
		function($call)
		{
			Model::object( 'PatientService', ['call' => $call ] );
		}
	],
	[
		'path' => '/patient-record/.+', 'call' =>
		function($call)
		{
			Model::object( 'PatientRecord', ['call' => $call ] );
		}
	],
	[
		'path' => '/service/.+', 'call' =>
		function($call)
		{
			Model::object( 'Service', ['call' => $call ] );
		}
	],
	[
		'path' => '/report/.+', 'call' =>
		function($call)
		{
			Model::object( 'Report', ['call' => $call ] );
		}
	],
	[
		'path' => '/branch-service/.+', 'call' =>
		function($call)
		{
			Model::object( 'BranchService', ['call' => $call ] );
		}
	],
	[
		'path' => '/service-requirement/.+', 'call' =>
		function($call)
		{
			Model::object( 'ServiceRequirement', ['call' => $call ] );
		}
	],
	[
		'path' => '/philhealth-transmittal/.+', 'call' =>
		function($call)
		{
			Model::object( 'PhilhealthTransmittal', ['call' => $call ] );
		}
	],
	[
		'path' => '/forms/.+', 'call' =>
		function($call)
		{
			Model::object( 'Forms', ['call' => $call ] );
		}
	],
	[
		'path' => '/payment/.+', 'call' =>
		function($call)
		{
			Model::object( 'Payment', ['call' => $call ] );
		}
	]
];


$route = new Route( $routes );
$route->init();

?>
