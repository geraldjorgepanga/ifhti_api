<?php


/**

    Index file of Chumon API

    @Author : Gerald Jorge Panga
    @Company: TTSS System Solutions | Thomas Group of Companies

    Copyright 2017 No Shave November

    #Requires:
        1) PDO
        2) DBLib        [Database Library]
        3) CryptoLib    [Cryptography Library]

**/

  header('Access-Control-Allow-Headers: Content-type, X-Requested-With ');
  header('Access-Control-Allow-Credentials: true');
  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Methods: POST,GET,OPTIONS,PUT,DELETE');
  header('Content-Type: text/event-stream');
  header('Cache-Control: no-cache');
  include_once 'Class/Route.php';
  include_once 'View/Encoder.php';
  include_once 'Class/CryptoLib.php';
  include_once 'Class/md5.php';
  include_once 'Class/DBLib.php';
  include_once 'Class/Time.php';
  include_once 'Class/AES.php';
  include_once 'Class/Curl.php';
  include_once 'Class/Settings.php';
  include_once 'Class/fpdf.php';
  require_once 'Class/Model.php';
  require_once 'Controller/Controller.php';

?>
