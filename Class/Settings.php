<?php

class Settings
{
    private static $table_name = 'tbl_settings';
    
    public static function get($value)
    {
        return DBLib::select(self::$table_name, $value)['data'][$value];
    }
    
    public static function match($array)
    {
        $keyMatched = null;
        
        $data = DBLib::select(self::$table_name, key($array))['data'][key($array)];
        
        (strcmp($data, $array[key($array)]) === 0) ? $keyMatched = true : $keyMatched = false;
        
        return $keyMatched;
    }
    
    public static function errorPage()
    {
        header('LOCATION: http://chumon.thethomasgc.com/chmn/View/error.php');
    }
	
	public static function checkArray($object)
	{
		if($object['count'] > 0)
		{
			if($object['count'] == 1)
			{
				$object['data'] = array($object['data']);
			}
		}else
		{
			$object['data'] = array();
		}
		
		return $object;
	}
	
	public static function hasAccess($requiredPostData)
	{
		$postData = null;
		if(Chumon::requiredPost($requiredPostData))
		{
			$postData = Chumon::getPostData();
			if( isset( $postData['api_key'] ) )
			{
				if(self::match(array('global_key' => $postData['api_key'])))
				{
					$response = $postData;
					$response['access'] = true;

					return $response;
				}else
				{
					$response['access'] = false;
					return $response;				
				}	
			}else
			{
					$response['access'] = false;
					return $response;	
			}
		}else
		{
				$response['access'] = false;
				return $response;				
		}
	}
	
	public static function isValidData($data)
	{
		$data = ( is_array( $data ) ) ? $data : [ $data ];
		$valid = true;
		foreach( $data as $value )
		{
			$valid = ( strlen($value) == 0 ) ?	false : $valid;
		}
		return $valid;
	}
	
	public static function coordinatesFormat($coords)
	{
		$prim = explode('.', $coords);
		//13
		$f = $prim[0];
		$s = $prim[1];
		$coordLen = strlen($s);
		$tempCoords = "";

		if($coordLen < 7)
		{
			$tempCoords = $s;
			for($i = $coordLen; $i < 7; $i++)
			{
				$tempCoords .= "0";
			}

			$s = $tempCoords;
		}

		if($coordLen > 7)
		{
			for($i = 0; $i < 7; $i++)
			{
				$tempCoords .= $s[$i];
			}	
			$s = $tempCoords;
		}

		return $f . "." .  $s;
	}
	
	public static function implodeDate()
	{
		$time = Time::getTime(8);
		$full = explode(' ', $time);
		$f = explode('-', $full[0]);
		$s = explode(':', $full[1]);
		$ff = implode('', $f);
		$ss = implode('', $s);
		$fin = $ss.$ff;
		
		return $fin;
	}
	
	public static function getObjectDate()
	{
		$datetime = explode( ' ', Time::getTime(8) );
		$date = explode( '-', $datetime[0] );
		return (object)[  'year' => $date[0], 'month' => $date[1], 'day' => $date[2] ];
	}

	/** 
		Required proper format 
		@ YYYY-MM-DD HH:mm:ss - String
	**/
	public static function toObjectDate( $DT )
	{
		$datetime = explode( ' ', $DT );
		$date = explode( '-', $datetime[0] );
		return (object)[  'year' => $date[0], 'month' => $date[1], 'day' => $date[2] ];
		
	}
	public static function getSeparator()
	{
		$separator = '/';
		$cwd = explode( $separator , getcwd() );
			
		return ( sizeof($cwd) > 1 ) ? $separator : '\\';
	
	}
}


?>