<?php

class Upload
{
	function __construct()
	{
		
	}
	function __call( $name, $args )
	{
		$this->upload( $name );
	}
	
	function upload($name)
	{
		//Required Branch ID;
			if($_FILES)
			{
				foreach($_FILES as $index=>$vals)
				{
					if( sizeof( explode('/', $index) ) > 1 )
					{
						$file_chunk = explode('/', $index);
						$_root_dir  = implode( 
							Settings::getSeparator(), array_merge([ getcwd(), 'Data', 'Reports', $file_chunk[1], $file_chunk[0] ])
						);
						$response = ['status' => 200, 'message' =>  "File Uploaded Successfully!"];

						if($_FILES[$index]['error'] > 0){
							$response = ['status' => 400, 'message' => 'An error ocurred when uploading.' ];
						}

						$type = explode( '.', $_FILES[$index]['name'] );
						

						$slash = Settings::getSeparator();
						$files = scandir(implode( $slash , [ getcwd(), 'Data', 'Reports', '001', '2018-05' ]));

						$toRemove = $name;
						$hasFile = false;
						foreach( $files as $file )
						{
							$chunk = explode( '.',  $file );

							if( $chunk[0] == $toRemove )
							{
								$toRemove = $file;
								$hasFile = true;
							}
						}
						
						if($hasFile)
						{
							unlink($_root_dir.Settings::getSeparator(). $toRemove );

							$response = ['status' => 200, 'message' => 'File overwritten.' ];
						}
						if(!move_uploaded_file(
							$_FILES[$index]['tmp_name'], $_root_dir.Settings::getSeparator(). $name . '.' . $type[ sizeof($type)-1 ] )
						  )
						{
							$response = ['status' => 400, 'message' =>'Error uploading file - check destination is writeable.'];

						}

						Encoder::set( $response );

					}

				}
			}else
			{
				Encoder::mpd();
			}
			
	}
	
	function uploadOtherDocs()
	{
		if( isset($_FILES['docs']) && isset($_POST['data']) )
		{
			$files = $this->sortFiles( 'docs' );
			$formData = json_decode($_POST['data'], true);
			$slash = Settings::getSeparator();
			$root = implode( $slash, array_merge([getcwd()], $formData['dir']) );

			$respond = [];
			foreach($files as $file)
			{
				$res = [];
				$res['name'] = $file['name'];
				$file_dir = $root . $slash . $file['name'];

				if( file_exists( $file_dir ) )
				{
					$res['status'] = 400; 
				}else
				{
					$res['status'] = ( move_uploaded_file($file['tmp_name'], $file_dir) ) ? 200 : 400;
				}
				array_push($respond, $res);
			}
			Encoder::set([
				'status' => 200,
				'message'=> 'Success',
				'data' => $respond
			]);
			
		}else
		{
			Encoder::mpd();
		}
	}
	function uploadPatientRecord()
	{
		if( isset($_FILES['record']) && isset($_POST['data']) )
		{
			$formData = json_decode($_POST['data'], true);
			$slash = Settings::getSeparator();
			$root = implode( $slash, array_merge([getcwd()], $formData['dir']) );
			$res = [];
			
			$file = $_FILES['record'];

			$filename = $file['name'];
			$chunk = explode( '.', $filename );

			$filetype = '.' . $chunk[ sizeof($chunk) - 1 ];
			$file_dir = $root . $slash . $formData['filename'] . $filetype;

			if( file_exists( $file_dir ) )
			{
				unlink( $file_dir );
			}
			
			$res['status'] = ( move_uploaded_file($file['tmp_name'], $file_dir) ) ? 200 : 400;
			$res['message'] = ($res['status'] == 200) ? 'Success Upload..' : $filetype;
			
			Encoder::set( $res );
		}else
		{
			Encoder::mpd();
		}		
	}
	function sortFiles($name)
	{
		$docs = $_FILES[$name];
		$files = [];
		for($i = 0; $i < sizeof($docs['name']); $i++)
		{
			array_push($files, [ 
				'name' => $docs['name'][$i],
				'type' => $docs['type'][$i],
				'tmp_name' => $docs['tmp_name'][$i],
				'error' => $docs['error'][$i],
				'size' => $docs['size'][$i]
			]);
		}

		return $files;
		
	}
	
	function uploadTransmittal()
	{
		if( isset($_FILES['pt']) && isset($_POST['data']) )
		{
			$formData = json_decode($_POST['data'], true);
			$slash = Settings::getSeparator();
			$root = implode( $slash, array_merge([getcwd()], $formData['dir']) );
			$res = [];
			
			$file = $_FILES['pt'];

			$filename = $file['name'];
			$chunk = explode( '.', $filename );

			$filetype = '.' . $chunk[ sizeof($chunk) - 1 ];
			$file_dir = $root . $slash . $formData['filename'] . $filetype;

			if( file_exists( $file_dir ) )
			{
				unlink( $file_dir );
			}
			
			$res['status'] = ( move_uploaded_file($file['tmp_name'], $file_dir) ) ? 200 : 400;
			$res['message'] = ($res['status'] == 200) ? 'Success Upload..' : 'Error Uploading..';
			
			Encoder::set( $res );
		}else
		{
			Encoder::mpd();
		}		
	}
}

?>