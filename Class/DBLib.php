<?php

/**
 * @author		Gerald Jorge R. Panga <geraldjorgepanga@gmail.com>
 * @copyright	Copyright (C), 2017-18 Gerald Panga
 * @license		GNU General Public License 3 (http://www.gnu.org/licenses/)
 *				Refer to the LICENSE file distributed within the package.
 *
 * @link		http://gjrpco.ueuo.com/geraldjorgepanga/
 *
 * @internal	Inspired by Junade Ali @ https://github.com/IcyApril/CryptoLib
 */


/**
 * Class DBLib (v0.1 No Shave November)
 * Created by Gerald Jorge R. Panga
 * Requires PHP 5.3.0+ , PHP Data Object (PDO)
 */

/*
    DBLib is an open-source PHP Database Library.
    Copyright (C) 2017  Gerald Jorge R. Panga

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */ 
  class DBLib
  {
    /*
        Private Variables in this class
            ---Important to manually change this settings for your project.
        
        Settings running on LOCAL MACHINE:
        @server_name as 'localhost'
        @username as 'root'
        @password as ''
        @database_name as 'your_db';
    */

    //Change this variable depending on your database driver being used;
    private static $database_driver = 'mysql';
    //Please change the server_name below, addressed to your server in use.
    private static $host_name = 'localhost';
    //private static $host_name = 'mysql.hostinger.com';
    //$username is required depending on your Hosting/Server
	private static $username = 'root';
    //$password is required depending on your Hosting/Server
    private static $password = ''; 
    //private static $password = 'fgsxDuEN67QvFDU';
    //database_name is required for every database you wish to modify/use.
    //private static $database_name = 'tgc_db';
    private static $database_name = 'ifhti_db';  
    /**
        Will return Connection to your Database
        @throw Exception
        @return connection
    **/
    public static function connect()
    {
        try 
        {   $PDO = new PDO
            ( 
                self::$database_driver.":host=".
                self::$host_name.";dbname=".
                self::$database_name, 
                self::$username, 
                self::$password
            );
            $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $PDO;
        }
        catch(PDOException $e)
        {
            return null;
        }
    }
            
    /**
        Select All Function in This DBLib
        -Generate Order By extension depending on database driver being use;
        
        @params [string] table_name;
        @return [array] response
    **/
    public static function selectAll($table_name)
    {
            $obj = null;    $idIndex  = 0;
            $PDO = self::connect();
            $preparedStmt = $PDO->prepare('SELECT * FROM ' . $table_name);
            $preparedStmt->execute(); 
            
            return self::fetcher($preparedStmt);
    }
      
    /**
        Creating Database Function in This DBLib Class
        
        @params database name
        @return array response
    **/
    public static function createDatabase($db_name)
    {
        try
        {
            $PDO = self::connect();
            $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);    
            $PDO->exec("CREATE DATABASE " . $db_name);
            $response['status'] = 200;
            $response['message']= 'Database ' . $db_name . ' was successfully created!';
        }
        catch(PDOException $e)
        {
            $response['status'] = 400;
            $response['message']= $e->getMessage();
        }
        
        return $response;
    }
      
    /**
        Counter Method in this DBLib Class
        -Accepts Query
    **/
    public static function getCount()
    {
        try
        {
            $PDO = self::connect();
            $bind = false;  $array_of_column_and_value = null;
            switch(func_num_args())
            {
                #Table Count
                case 1: 
                    $selectQuery = self::selectQuery(func_get_arg(0), array('*'));
                    break;
                case 2:
                    $selectQuery = self::selectQuery(func_get_arg(0), array('*'));
                    $array_of_column_and_value = func_get_arg(1);
                    $selectQuery.= self::whereClause($array_of_column_and_value);
                    $bind = true;
                    break;
            }


            $PS = $PDO->prepare($selectQuery);

            if($bind){   $PS->execute( self::parse($array_of_column_and_value) );   }
            else{ $PS->execute();  }
            
            $response = self::statusObject(200);
            $response['data']   =   $PS->rowCount();
        }
        catch(PDOException $e)
        {
            $response = self::statusObject(400, $e->getMessage());
        }
        
        return $response;
    }
    
      
      
    /**
        Get Row in this DBLib Class
        -Get the row from the executed statement
    **/  
    public static function getRow($data, $obj)
    {
        foreach($data as $d)
        {   foreach($d as $a)
            {   $obj['data'][key($d)] = $a;
                next($d);
            }
        }
        return $obj;
    }
      
      
    /**
        Get List in this DBLib Class
        -Get the multiple rows from the executed statement
    **/  
    public static function getList($data, $obj)
    {
        $index = 0;
        foreach($data as $d)
        {   foreach($d as $a)
            {   $obj['data'][$index][key($d)] = $a;
                next($d);
            }
            $index++;
        }
        return $obj;
    }
      
	/**
        Getting Database Name Function in This DBLib Class
        
        @returns database name
    **/ 
	public static function dbname()
	{
		return self::$database_name;
	}
	  
 	/**
        Getting Defined Table Columns and Attributes Function in This DBLib Class
        
        @returns table properties
    **/ 
	public static function tableProperties($table_name)
	{
		$data = [];
		try
		{
			$PDO = self::connect();
            $preparedStmt = $PDO->prepare('DESCRIBE ' . $table_name );
            $preparedStmt->execute(); 
			$data =  $preparedStmt->fetchAll(PDO::FETCH_ASSOC);
			$response = [ 'status' => 200, 'message' => 'success', 'data' => $data, 'count' => sizeof( $data ) ];
		}catch(PDOException $e)
		{
			$response = [ 'status' => 200, 'message' => $e->getMessage(), 'count' => sizeof( $data ) ];			
		}
		
		return $response;
	}	  

    /**
        Fetcher Function in This DBLib
        -Accepts prepared statement created by PDO and Returns Array;
        
        @params PDO->prepare statement  $prepared_statement;
        
        @return [array] data
    **/
    public static function fetcher($prepared_stmt)
    {
        try
        {   
            $response = null;
            
            $data = $prepared_stmt->fetchAll(PDO::FETCH_ASSOC);
            $rowCount = $prepared_stmt->rowCount();
            $response['status'] = 200;
            
            if($rowCount > 0)
            {
                $response['message'] = 'Data successfully fetched!';
                ($rowCount === 1) ? $response = self::getRow($data, $response)
                    :   $response = self::getList($data, $response);
            }
            else
            {
                $response['message'] = 'No records was found!';
            }
            
            $response['count'] = $rowCount;
            return $response;

        }catch(PDOException $e)
        {
            return self::statusObject(400, $e->getMessage());
        }
    }
    /**
        Insert function in this DBLib Class
        
        @params table_name 
        @params array_of_column_and_values
        
        @throw  Exeption
        @returns array $repsonse
    **/  
    public static function insert($table_name, $array_of_column_and_values)
    {
        try
        {
            $PDO = self::connect();
            $insertQuery = self::insertQuery($table_name, $array_of_column_and_values);
            $PS  = $PDO->prepare($insertQuery);
            $PS->execute(self::parse($array_of_column_and_values));
            $response = self::statusObject  (200);
            
        }catch(PDOException $e)
        {
            $response = self::statusObject(400, $e->getMessage());
        }
        
        return $response;

    }
      
    /**
        Parsing function in this DBLib Class
        
        @params array_table 
        @returns string $string / array
    **/  
    public static function parse()
    {  
        $index_to_parse = null;
        switch(func_num_args())
        {
            case 1:
                $param_array = func_get_arg(0);
                foreach($param_array as $array_value)
                { 
                    $new_array[':' . key($param_array) ] = $array_value;    
                    next($param_array); 
                }
                return $new_array;
            case 2: 
                $array_value = func_get_arg(0);
                $char= func_get_arg(1);
                $add = '';
                
                break;
            case 3:
                $array_value = func_get_arg(0);
                $char = func_get_arg(1);
                $add= func_get_arg(2);
                break;
            case 4:
                $array_value = func_get_arg(0);
                $char = func_get_arg(1);
                $add= func_get_arg(2);
                $index_to_parse = func_get_arg(3);
                break;
        }
        $values = null; $valuesIndex = 0; 
        foreach($array_value as $val)
        {
            if($index_to_parse > 0)
            {
                $values[$valuesIndex] = $add . key($array_value);
                $values[$valuesIndex + 1] = $char;
                $valuesIndex+=2;                    
            }
            else
            {
                $values[$valuesIndex] = $add . $val;
                $values[$valuesIndex + 1] = $char;
                $valuesIndex+=2;                    
            }
            
            next($array_value);
        }
        $string = null;
        for($i = 0; $i < sizeof($values) - 1; $i++)
        {
            $string .= $values[$i];
        }
        return $string;
    }
      

    public static function isArray($value)
    {
        if(!is_array($value))
        {
            return array($value);
        }else
        {
            return $value;
        }
        
    }
    
    /**
        Select Method in this DBLib Class
        
        @Overload Function
        @Number of ARGS
         --------------------------------------
        |NO of Args |   PARAMS      |   Return |
        |---------------------------------------
        |   2       |table_name     |   array  |
        |           |array[col]     |          |
        |---------------------------------------
        |   3       |table_name     |          |
        |           |array[col]     |   array  |
        |           |array[col][val]|          |
        ----------------------------------------
        |   4       |table_name     |          |
        |           |array[col]     |   array  |
        |           |array[col][val]|          |
        |           |col:ORDER      |          |
        ----------------------------------------
    **/
    public static function select()
    {
        try
        {
            $select_query = null;   $col_name = null;
            $where_clause = null;   $order_by = null;       
            $table_name = null;     $bind = false;
            switch(func_num_args())
            {
                case 1: #Accepts table_name
                    $table_name = func_get_arg(0);
                    $select_query .= self::selectQuery($table_name, array("*"));
                    break;
                case 2: #Accepts table_name, col_name;
                    $table_name = func_get_arg(0);
                    $col_val = self::isArray(func_get_arg(1));
                    $select_query .= self::selectQuery($table_name, $col_val);
                    break;
                case 3: #Accepts table_name, col_name, where_clause;
                    $table_name = func_get_arg(0);
                    $col_name   = self::isArray(func_get_arg(1));
                    $where_clause   = func_get_arg(2);
                    $select_query .= self::selectQuery($table_name, $col_name);
                    ($where_clause == 0) ? $select_query .= ' ' : $select_query .= self::whereClause($where_clause);
                    ($where_clause == 0) ? $bind = false : $bind = true;
                    break;
                case 4: #Accepts table_name, col_name, where_clause, order;
                    $table_name = func_get_arg(0);
                    $col_name   = self::isArray(func_get_arg(1));
                    $where_clause   = func_get_arg(2);
                    $order  =   func_get_arg(3);
                    $select_query .= self::selectQuery($table_name,$col_name);
                    ($where_clause == 0) ? $select_query .= ' ' : $select_query .= self::whereClause($where_clause);
                    ($where_clause == 0) ? $bind = false : $bind = true;
                    $select_query .= self::orderBy($order);
                    
                    break;
                case 5: #Accepts table_name, col_name, where_clause, order, limit;
                    $table_name     = func_get_arg(0);
                    $col_name       = self::isArray(func_get_arg(1));
                    $where_clause   = func_get_arg(2);
                    $order          = func_get_arg(3);
                    $limit          = func_get_arg(4);
                    $select_query .= self::selectQuery($table_name,$col_name);
                    ($where_clause == 0) ? $select_query .= ' ' : $select_query .= self::whereClause($where_clause);
                    ($where_clause == 0) ? $bind = false : $bind = true;
                    $select_query .= self::orderBy($order);
                    $select_query .= self::limitBy($limit);
                    break;
            }

            $PDO = self::connect();
            $preparedStmt = $PDO->prepare($select_query);

            if($bind)
            {
                $preparedStmt->execute($where_clause);            
            }
            else
            {
                $preparedStmt->execute();
            }


            return self::fetcher($preparedStmt);

        }
        catch(PDOException $e)
        {
            return self::statusObject(400, $e->getMessage());
        }
        
    }
      
      
      
    /**
        Update Method in This DBLib
        @ Accepts 3 parameters
            *table_name
            *update_col_and_value
            *pointer_col_and_value
    **/
    public static function update($table_name, $update_col_and_value, $pointer_col_and_value)
    {
        
        $update_query = null;
        $array_values = array_merge($update_col_and_value, $pointer_col_and_value);
        
        try
        {
            $update_query =  self::updateQuery($table_name, $update_col_and_value, $pointer_col_and_value);
            $PDO = self::connect();
            $PS = $PDO->prepare($update_query);
            $PS->execute(self::parse($array_values));
            return self::statusObject(200, 'Data successfully updated');
        }catch(PDOException $e)
        {
            return self::statusObject(400, $e->getMessage());
        }
    }
      
      
    /**
        Insert Query Function in This DBLib
        -Generate query depending on database driver being use;
        
        @params array_of_col_and_value;
        @params table_name;
        
        @return string
    **/
    public static function insertQuery($table_name, $array_of_col_and_value)
    {
        switch(self::$database_driver)
        {
            case 'mysql': 
                $query = 
                    "INSERT INTO $table_name (" . self::parse($array_of_col_and_value, ',', ' ', 1) .
                    ") VALUES (" . self::parse($array_of_col_and_value, ',', ':', 1). ")";
                
                return $query;
        }
    }  
    
      
    /**
        Select Query Function in This DBLib
        -Generate query depending on database driver being use;
        
        @params array_of_col_and_value;
        @params table_name;
        
        @return string
    **/
    public static function selectQuery($table_name, $array_of_col_and_value)
    {
        switch(self::$database_driver)
        {
            case 'mysql': return "SELECT " . self::parse($array_of_col_and_value, ', ') . " FROM " . $table_name;
        }
    }
      
    /**
        Update Query Function in This DBLib
        -Generate Order By extension depending on database driver being use;
        
        @params string update;
        
        @return string
    **/      
    public static function updateQuery($table_name, $update_col_and_value, $pointer_col_and_value)
    {
        $update_query = null;
        switch(self::$database_driver)
        {
            case 'mysql' :
                $tempArray = null;
                for($i = 0; $i < sizeof($update_col_and_value); $i++)
                {
                    $tempArray[$i] = key($update_col_and_value) . '= :' . key($update_col_and_value);
                    next($update_col_and_value);
                }
                $update_query .= "UPDATE $table_name SET " . implode(', ' , $tempArray);
				
                $update_query .= self::whereClause($pointer_col_and_value);
                break;
        }
        
        return $update_query;
    }
      
    /**
        Order By Function in This DBLib
        -Generate Order By extension depending on database driver being use;
        
        @params string order_clause;
        
        @return string
    **/
    public static function orderBy($order_clause)
    {
        $orderBy = explode(':', $order_clause);
        
        switch(self::$database_driver)
        {
            case 'mysql':   return " ORDER BY " . $orderBy[0]  .  ' ' . $orderBy[1];
            case 'oracle':  break;
        }
        
    }
      
    /**
        Limit By Function in This DBLib
        -Generate Limit By extension depending on database driver being use;
        
        @params string count;
        @return string
    **/   
    public static function limitBy($count)
    {
        switch(self::$database_driver)
        {
            case 'mysql':   return " LIMIT " . $count;
            case 'oracle':  break;
        }
    }
      
      
    /**
        Where Clause Function in This DBLib
        -Generate Where CLause depending on database driver being use;
        
        @params [array] array_of_col_and_value;
        
        @return string
    **/
    public static function whereClause()
    {
        $array_index = 0;   $equal = '='; $comma = ',';
        $expression = ' AND ';
        $array_of_col_and_value = func_get_arg(0);
        
        if( func_num_args() == 2)
        {
            $expression = func_get_arg(1);
        }

        #Parse Array To New Array
        foreach($array_of_col_and_value as $value)
        {
            $new_array[$array_index] = key($array_of_col_and_value) . $equal . ' :' . key($array_of_col_and_value) ;
            $array_index++;
            next($array_of_col_and_value);
        }

        switch(self::$database_driver)
        {
            case 'mysql': return " WHERE " . self::parse($new_array, $expression);
        }
    }
      
      
    
    /**
        Execute methods in this DBLib Class
        -This will execute the entered parameter (query)
        
        @params string query
        @params array column_and_value
        
        @return array statusObject
    **/  
    public static function execute()
    {
        $query = func_get_arg(0);
        $arrQuery = explode(' ', $query);
        
        try
        {
            $PDO = self::connect();
            $PS = $PDO->prepare($query);
            
            if(func_num_args() > 1)
            {
                $PS->execute(self::parse(func_get_arg(1)));
            }
            else
            {
                $PS->execute();
            }
            
            if($arrQuery[0] == 'SELECT')
            {
                $obj  = self::fetcher($PS);   
            }
            else
            {
                $obj = self::statusObject(200, 'Successfully Executed');
            }
        }catch(PDOException $e)
        {
            $obj['status'] = 400;
            $obj['message']= $e->getMessage();
        }
        return $obj;
    }

    
    /**
        Status Object Method in this DBLib Class
        @Overload Method
        @param  int     status
        @param  string  message
    **/
    public static function statusObject()
    {
        $status  = func_get_arg(0);
        $message = null;
        
        if(func_num_args() == 2){   $message = func_get_arg(1);    }
        else{   $message = 'Successfully Executed!';    }
        
        switch($status)
        {
            case 200:   return array('status' => 200, 'message' => $message);
            case 400:   return array('status' => 400, 'message' => $message);
        }
    }
	  
	  	public static function multiple($object)
	{
		if(isset($object->count))
		{
			if($object->count > 0)
			{
				if($object->count == 1)
				{
					$object->data = [$object->data];
				}
			}else
			{
				$object->data =[];
			}

			return $object;			
		}else
		{
			if( is_array($object) )
			{
				echo "is array";
				return (object)$object;
			}else
			{
				$object = (object)$object;
				$object->data = [ $object ];
				return $object;
			}
		}
	}
	  
	/**
		Accepts: 
		-tablename
		-Array of date [ 'from' => string, 'to' => string, 'column' => string ]
	**/
	public static function dateRange( $table_name, $array_of_date )
	{
		
	}
	  
	public static function object( $array_of_values )
	{
		return (object)$array_of_values;
	}
	  
	public static function primaryKey( $table_name )
	{
		$data =  self::tableProperties( $table_name );
		$data = self::multiple( $data );
		$primary_key_pointer = 'PRI';
		$primary_key = null;
		foreach( $data->data as $col )
		{
			$col = self::object( $col );
			$primary_key = ( $col->Key == $primary_key_pointer ) ?  $col->Field : $primary_key;
		}
		return $primary_key;
	}	
	 
	public static function foreignKey($table_name)
	{	$fk_pointer = 'MUL';
		$data =  self::tableProperties( $table_name );
		$data = self::multiple( $data );
		$fk_list = [];
		foreach( $data->data as $col )
		{
			$col = self::object( $col );
			if( $col->Key == $fk_pointer )
			{
				array_push( $fk_list, $col->Field );
			}
		}
		return $fk_list;		
	}
      
	public static function toArray($object)
	{
		if($object['count'] > 0)
		{
			if($object['count'] == 1)
			{
				$object['data'] = array($object['data']);
			}
		}else
		{
			$object['data'] = [];
		}
		
		return $object;
	}
	  
	public static function betweenDates( $table_name, $col, $from, $to )
	{
		try
		{
			$select = self::selectQuery($table_name, ['*']);
			switch(self::$database_driver)
			{
				case 'mysql': $where = " WHERE $col BETWEEN '$from' AND '$to'";
			}
			$PDO = self::connect();
            $preparedStmt = $PDO->prepare($select . $where);
			$preparedStmt->execute();
            return self::toArray(self::fetcher($preparedStmt));
        }
        catch(PDOException $e)
        {
            return self::statusObject(400, $e->getMessage());
        }
	}

    


  }

 ?>
