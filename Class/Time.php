<?php

    class Time
    {
        #---@ Author : Gerald Jorge R. Panga
        #---@ Created: 10/23/2017 Philippines
        public static function getTime($timezone)
        {
            return gmdate("Y-m-d H:i:s", time() + 3600 * $timezone); 
        }
		
		public static function implodedTime()
		{
			$datetime = explode( ' ', self::getTime( 8 ) );
			$timestamp = implode( '', [ implode( '', explode( ':', $datetime[1] ) ), implode( '', explode( '-', $datetime[0] ) ) ] );
			
			return $timestamp;
		}
		
		public static function datetimeToString()
		{
			return gmdate( "d-M-Y" , time() + 3600 * 8);
		}
		
		public static function toMonthString( $month )
		{
			$months = [
				'01' => 'JANUARY','02' => 'FEBRUARY', '03' => 'MARCH', '04' => 'APRIL', '05' => 'MAY', '06' => 'JUNE',
				'07' => 'JULY', '08' => 'AUGUST', '09' => 'SEPTEMBER', '10' => 'OCTOBER', '11' => 'NOVEMBER', '12' => 'DECEMBER'
			];
			foreach( $months as $key=>$val )
			{	
				if( $key == $month )
				{
					return $val;
				}
			}
			return 'Undefined';
		}
		
		public static function toString( $datetime )
		{
			$datetime = explode( ' ', $datetime );
			$date = explode( '-', $datetime[0] );
			
			return implode( '-', [ $date[0], self::toMonthString( $date[1] ), $date[2] ] );
		}
		
		public static function chunk( $datetime )
		{
			$dateTime = explode( ' ', $datetime );
			
			if( sizeof($dateTime) > 1 )
			{
				$date = $dateTime[0];
				$time = $dateTime[1];

				$dateChunks = explode( '-', $date );
				$year = $dateChunks[0];
				$month = $dateChunks[1];
				$day = $dateChunks[2];

				$timeChunks = explode( ':', $time );
				$hour = $timeChunks[0];
				$minute = $timeChunks[1];
				$second = $timeChunks[2];

				return (object)[
					'date' => $date,
					'time' => $time,
					'year' => $year,
					'month' => $month,
					'day' => $day,
					'hour' => $hour,
					'minute' => $minute,
					'second' => $second
				];
				
			}else
			{
				//Check if Time Or Date
				$check = explode('-', $datetime);
				if(sizeof($check) > 1)
				{
					//Date
					$dateChunks = explode( '-', $datetime );
					$year = $dateChunks[0];
					$month = $dateChunks[1];
					$day = $dateChunks[2];
					
					return (object)[
					'date' => $datetime,
					'time' => '00:00:00',
					'year' => $year,
					'month' => $month,
					'day' => $day,
					'hour' => '00',
					'minute' => '00',
					'second' => '00'
					];
				}else
				{
					//Time
					$timeChunks = explode( ':', $datetime );
					$hour = $timeChunks[0];
					$minute = $timeChunks[1];
					$second = $timeChunks[2];
					
					return (object)[
					'date' => '0000-00-00',
					'time' => $datetime,
					'year' => '0000',
					'month' => '00',
					'day' => '00',
					'hour' => $hour,
					'minute' => $minute,
					'second' => $second
					];
				}
			}
		}
		

    }



?>