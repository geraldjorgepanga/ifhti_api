<?php

/**
    Requires:
        DBLib Class
**/
class Model
{
    
    private static $classesDirectory = 'Model/';
    private static $libraryDirectory = 'Class/';
	private static $interfaceDirectory = 'Interface/';
    private static $executeIndexName = 'call';
    private static $classExtension   = '.php';
	private static $parentClass 	 = '_init.php';
    public static function object()
    {
		include_once self::$classesDirectory.self::$parentClass;
        $class_name = func_get_arg(0);
        require_once self::$classesDirectory . $class_name . self::$classExtension;
                
        if(func_num_args() > 1)
        {
            $Params = func_get_arg(1);
            $Class = new $class_name($Params);

            if(array_key_exists(self::$executeIndexName, $Params))
            {
                $Class->{ $Params[self::$executeIndexName] }();
            }else
            {
                return $Class;
            }
        }else
        {
            $Class = new $class_name();
            
            return $Class;
        }
    }

    public static function includes()
    {
        $class_name = func_get_arg(0);
        include_once self::$classesDirectory . $class_name . self::$classExtension;
    }
    
    public static function requires()
    {
        $class_name = func_get_arg(0);
        require_once self::$classesDirectory . $class_name . self::$classExtension;
    }
    
    public static function library()
    {
        $class_name = func_get_arg(0);
        require_once self::$libraryDirectory . $class_name . self::$classExtension;
                
        if(func_num_args() > 1)
        {
            $Params = func_get_arg(1);
            $Class = new $class_name($Params);

            if(isset($Params[self::$executeIndexName]))
            {
                $Class->{ $Params[self::$executeIndexName] }();
            }else
            {
                return $Class;
            }
        }else
        {
            $Class = new $class_name();
            
            return $Class;
        }
    }
	
	public static function interface()
    {
        $class_name = func_get_arg(0);
        require_once self::$interfaceDirectory . $class_name . self::$classExtension;
                
        if(func_num_args() > 1)
        {
            $Params = func_get_arg(1);
            $Class = new $class_name($Params);

            if(isset($Params[self::$executeIndexName]))
            {
                $Class->{ $Params[self::$executeIndexName] }();
            }else
            {
                return $Class;
            }
        }else
        {
            $Class = new $class_name();
            
            return $Class;
        }
    }
    
    public static function requiredPost($post_data)
    {
        $set   = true;
        
        (is_array($post_data)) ? : $post_data = array($post_data);

        foreach($post_data as $index)
        {
            (isset($_POST[$index])) ? : $set = false;
        }
        return $set;
    }
    
    public static function requiredGet($get_data)
    {
        $set = true;
        
        (is_array($get_data)) ? : $get_data = array($get_data) ;
        
        foreach($get_data as $index)
        {
            (isset($_GET[$index])) ? : $set = false;
        }
        return $set;
    }
	
	public static function requiredPut($put_data)
	{
		$set = true;
		$PUT = self::getPutData();
		(is_array($put_data)) ? : $put_data = array($put_data) ;
        foreach($put_data as $index)
        {
			(isset($PUT[$index])) ? : $set = false;
		}
		return $set;
	}
         
	
	public static function getPutData()
	{
		$request_payload = file_get_contents('php://input');
		$data = json_decode( json_encode( json_decode( $request_payload ) ) , true );
		
		return $data;
	}
	
    public static function getPostData()
    {
        return $_POST;
    }
	
	         
    public static function getGetData()
    {
        return $_GET;
    }
    
    public static function loadData($tableName, $primaryKey, $array)
    {
        if(array_key_exists($primaryKey, $array))
        {
            $param = array($primaryKey => $array[$primaryKey]);

            $response = DBLib::select($tableName, '*', $param);

            return $response['data'];
        }else
        {
            return $array;
        }
    }
	public static function isAuthorized($key)
	{

	}
         
}
?>