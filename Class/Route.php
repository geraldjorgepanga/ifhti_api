<?php

class Route
{
	/**
	* @var array $_listUri List of URI's to match against
	*/
	private $_listUri = [];
	
	/**
	* @var array $_listCall List of closures to call 
	*/
	private $_listCall = [];
	
	/**
	* @var string $_trim Class-wide items to clean
	*/
	private $_trim = '/\^$';
		
	
	
	public function __construct($routes)
	{
		foreach($routes as $route)
		{
			$object = (object)$route;
			
			$uri = $object->path; 
			$function = $object->call;

			$uri = trim($uri, $this->_trim);
			$this->_listUri[] = $uri;
			$this->_listCall[] = $function;
		}
	}	
	/**
	* submit - Looks for a match for the URI and runs the related function
	*/
	public function init()
	{	
		$uri = isset($_REQUEST['uri']) ? $_REQUEST['uri'] : '/';
		$uri = trim($uri, $this->_trim);

		$replacementValues = array();

		/**
		* List through the stored URI's
		*/
		$matched = false;
		foreach ($this->_listUri as $listKey => $listUri)
		{
			/**
			* See if there is a match
			*/
			
			if($matched)
			{
				
			}else
			{
				if (preg_match("#^$listUri$#", $uri))
				{
					$matched = true;
					/**
					* Replace the values
					*/
					$realUri = explode('/', $uri);
					$fakeUri = explode('/', $listUri);

					/**
					* Gather the .+ values with the real values in the URI
					*/
					foreach ($fakeUri as $key => $value) 
					{
						if ($value == '.+') 
						{	$matched = true;
							$replacementValues[] = $realUri[$key];
							
						}
					}

					/**
					* Pass an array for arguments
					*/
					call_user_func_array($this->_listCall[$listKey], $replacementValues);
				}
				
			}
		}
		
	}
	
}











