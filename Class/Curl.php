<?php 
	class curl
	{
		/**
			Required args
			@url, @params, 
		**/
		private static $url = "http://localhost/alfms";
		
		public static function post( $url, $params )
		{
			$curl = curl_init( self::$url . $url );
			curl_setopt_array( $curl, [ 
				CURLOPT_HEADER => 0,
				CURLOPT_POST => 1,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_POSTFIELDS => $params
			]);
			$response = curl_exec( $curl );
			curl_close( $curl );
			
			return $response;
		}
		
		public static function get( $url, $params )
		{
			$query = implode( '?', [ self::$url . $url , http_build_query( $params ) ] );
			$curl = curl_init( $query );
			curl_setopt_array( $curl, [ 
				CURLOPT_HEADER => 0,
				CURLOPT_POST => false,
				CURLOPT_RETURNTRANSFER => 1
			]);
			$response = curl_exec( $curl );
			curl_close( $curl );
			
			return $response;			
		}
		
		public static function queryString( $object )
		{
			return http_build_query( $object );
		}
		
		public static function put( $url, $params )
		{
			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "PUT",
			  CURLOPT_POSTFIELDS => json_encode($params),
			  CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/json"
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);

			if ($err) {
			  echo "cURL Error #:" . $err;
			} else {
			  echo $response;
			}
			
			
		}
	}
	
	
	
?>