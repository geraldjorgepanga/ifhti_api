<?php

	class Branch extends _init
    {
		function __construct()
		{
			//Initialize Object;
			$this->init();
		}
		function init()
		{
			$this->tableName = "tbl_branch";
			/** This variable is used in generating Unique ID for these Class **/
			$this->prefix = "BRCH";
			/** These variables are important for fetching complete data; **/
			$this->pk = "b_id";
			$this->fk = [];
			/** Unique ID Added Random String Length **/
			$this->gidlen = 3;	
					   /**  
		   		Initialize Required Post Data  
			    $this->int->struct is the DEFAULT value every time ADD is called
			**/
  			$this->int = ( object )
  			[
  				'struct' => [
                            $this->pk => $this->branchId() ,
                            'b_name' => '',
                            'b_street' => '',
                            'b_barangay' => '',
                            'b_city_mun' => '',
                            'b_province' => '',
							'b_zip' => '',
							'b_created' => Time::getTime(8),
							'b_hid' => false
  						 ],
                  'add' => ['b_name', 'b_street', 'b_barangay', 'b_city_mun', 'b_province', 'b_zip' ],
			   'update' => [ $this->pk, 'b_name', 'b_street', 'b_barangay', 'b_city_mun', 'b_province', 'b_zip', 'b_hid' ],
			   'delete' => [ $this->pk ] ,
			   'get'	=> [ 'method', 'params' ],
			   'all'	=> ['chunk']
  			];
		}
		function branchId()
		{
			//Get Count
			$branch_list = DBLib::getCount( $this->tableName );
			
			$counter = $branch_list['data'] + 1;
			$prefix = "";

			for($i = strlen($counter) ; $i < 4; $i++)
			{
				$prefix .= "0";
			}

			return $prefix . $counter;
		}
		function all()
		{
			if( Model::requiredPost( $this->int->all ) )
			{
				$postData = Model::getPostData();
				
				if($this->isAuthorized( $postData['chunk'] ))
				{
					$this->printAllData();					
				}else
				{
					Encoder::set(['status' => 400, 'message'=> 'You are not authorized!']);
				}

			}else
			{
				Encoder::invalid();
			}
		}
	}
?>
