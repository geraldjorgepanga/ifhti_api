<?php

	class PatientServiceRecord extends _init
    {
		function __construct()
		{
			//Initialize Object;
			$this->init();
		}
		function init()
		{
			$this->tableName = "tbl_patient_service_record";
			/** This variable is used in generating Unique ID for these Class **/
			$this->prefix = "PSR";
			/** These variables are important for fetching complete data; **/
			$this->pk = "psr_id";
			$this->fk = [
				['pk' => 'ps_id', 'model' => 'PatientService', 'table' => 'patient_service_record'],
				['pk' => 'sr_id', 'model' => 'ServiceRecord', 'table'=> 'service_record']
			];
			/** Unique ID Added Random String Length **/
			$this->gidlen = 3;	
					   /**  
		   		Initialize Required Post Data  
			    $this->int->struct is the DEFAULT value every time ADD is called
			**/
  			$this->int = ( object )
  			[
  				'struct' => [
                            $this->pk => $this->generateId() ,
                            'ps_id' => '',
                            'sr_id' => '',
                            'psr_status' => 'Not Available',
                            'psr_created' => Time::getTime(8),
							'psr_updated' => ''
  						 ],
                  'add' => ['ps_id', 'sr_id', 'psr_status'],
			   'update' => [ $this->pk, 'ps_id', 'sr_id', 'psr_status', 'psr_updated'],
			   'delete' => [ $this->pk ] ,
			   'get'	=> [ 'method', 'params' ],
			   'all'	=> [ 'chunk' ]
  			];
		}
		function all()
		{
			if( Model::requiredPost( $this->int->all ) )
			{
				$postData = Model::getPostData();
				
				if($this->isAuthorized( $postData['chunk'] ))
				{
					$this->printAllData();					
				}else
				{
					Encoder::set(['status' => 400, 'message'=> 'You are not authorized!']);
				}

			}else
			{
				Encoder::invalid();
			}
		}
	}
?>
