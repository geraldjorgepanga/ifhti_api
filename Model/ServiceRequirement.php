<?php

	class ServiceRequirement extends _init
    {
		function __construct()
		{
			//Initialize Object;
			$this->init();
		}
		function init()
		{
			$this->tableName = "tbl_service_requirement";
			/** This variable is used in generating Unique ID for these Class **/
			$this->prefix = "SRQ";
			/** These variables are important for fetching complete data; **/
			$this->pk = "srq_id";
			$this->fk = [
				['pk' => 's_id', 'model' => 'Service', 'table' => 'service']
			];
			/** Unique ID Added Random String Length **/
			$this->gidlen = 3;	
					   /**  
		   		Initialize Required Post Data  
			    $this->int->struct is the DEFAULT value every time ADD is called
			**/
  			$this->int = ( object )
  			[
  				'struct' => [
                            $this->pk => $this->generateId(),
                            's_id' => '',
                            'srq_name' => '',
							'srq_created' => Time::getTime(8)
  						 ],
                  'add' => ['s_id', 'srq_name'],
			   'update' => [ $this->pk, 'srq_name'],
			   'delete' => [ $this->pk ] ,
			   'get'	=> [ 'method', 'params' ],
			   'all'	=> [ 'chunk' ]
  			];
		}
		function all()
		{
			if( Model::requiredPost( $this->int->all ) )
			{
				$postData = Model::getPostData();
				
				if($this->isAuthorized( $postData['chunk'] ))
				{
					$this->printAllData();					
				}else
				{
					Encoder::set(['status' => 400, 'message'=> 'You are not authorized!']);
				}

			}else
			{
				Encoder::invalid();
			}
		}
	}
?>
