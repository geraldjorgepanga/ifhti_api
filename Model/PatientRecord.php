<?php

	class PatientRecord extends _init
    {
		function __construct()
		{
			//Initialize Object;
			$this->init();
		}
		function init()
		{
			$this->tableName = "tbl_patient_record";
			/** This variable is used in generating Unique ID for these Class **/
			$this->prefix = "PR";
			/** These variables are important for fetching complete data; **/
			$this->pk = "pr_id";
			$this->fk = [
				[ 'pk' => 'b_id', 'model' => 'Branch', 'table' => 'branch' ],
				[ 'pk' => 'p_id', 'model' => 'Patient', 'table' => 'patient' ]
			];
			/** Unique ID Added Random String Length **/
			$this->gidlen = 3;	
					   /**  
		   		Initialize Required Post Data  
			    $this->int->struct is the DEFAULT value every time ADD is called
			**/
  			$this->int = ( object )
  			[
  				'struct' => [
                            $this->pk => $this->generateId() ,
                            'p_id' => '',
                            'b_id' => '',
                            'pr_date' => '',
                            'pr_created' => Time::getTime(8)
  						 ],
                  'add' => ['p_id', 'b_id', 'pr_date' ],
			   'update' => [ $this->pk, 'p_id', 'b_id', 'pr_date' ],
			   'delete' => [ $this->pk ] ,
			   'get'	=> [ 'method', 'params' ]
  			];
			
			$this->after_action = 'createFile';
		}
		
		function recordCountByBranch( $b_id )
		{
			$dblib_response = DBLib::select($this->tableName, '*', ['b_id' => $b_id]);
			
			return $dblib_response['count'];
		}
		
		function recordCountByDate( $date )
		{
			$dblib_response = DBLib::select($this->tableName, '*', ['pr_date' => $date]);
			return $dblib_response['count'];	
		}
		
		function recordCountByBranchAndDate( $branch ,$date )
		{
			$date = explode('-' , $date);
			$pref = implode('-' , [$date[0], $date[1]]);
			$query ="SELECT * FROM $this->tableName WHERE pr_date LIKE '%$pref%' AND b_id='$branch'"; 
			$dblib_response = DBLib::execute($query);

			return $dblib_response['count'];	
		}
		function createFile()
		{
			if( isset($this->postData['b_id']) && isset($this->postData['p_id']) )
			{
				$file = Model::object( 'File' );
				$file->createPatientFile( $this->postData['b_id'], $this->postData['p_id'] );
			}
		}
	}
?>
