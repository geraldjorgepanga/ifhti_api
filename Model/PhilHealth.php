<?php

	class PhilHealth extends _init
    {
		function __construct()
		{
			//Initialize Object;
			$this->init();
		}
		function init()
		{
			$this->tableName = "tbl_philhealth";
			/** This variable is used in generating Unique ID for these Class **/
			$this->prefix = "PH";
			/** These variables are important for fetching complete data; **/
			$this->pk = "ph_id";
			$this->fk = [
				[ 'pk' => 'p_id', 'model' => 'Patient', 'table' => 'patient' ]
			];
			/** Unique ID Added Random String Length **/
			$this->gidlen = 3;	
			/**  
		   		Initialize Required Post Data  
			    $this->int->struct is the DEFAULT structure and value;
			**/
  			$this->int = ( object )
  			[
  				'struct' => [
                            $this->pk => $this->generateId() ,
                            'p_id' => '',
                            'ph_no' => '',
                            'ph_date_applied' => '',
                            'ph_created' => Time::getTime(8)
  						 ],
                  'add' => ['p_id', 'ph_no', 'ph_date_applied'],
			   'update' => [ $this->pk, 'p_id', 'ph_no', 'ph_date_applied'],
			   'delete' => [ $this->pk ],
			   	  'get' => [ 'method', 'params' ],
		   	 'required' => ['chunk']
  			];
		}				
	}
?>
