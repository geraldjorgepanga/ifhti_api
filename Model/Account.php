<?php

	class Account extends _init
    {
		function __construct()
		{
			//Initialize Object;
			$this->init();
		}
		function init()
		{
			$this->tableName = "tbl_branch_account";
			/** This variable is used in generating Unique ID for these Class **/
			$this->prefix = "ACC";
			/** These variables are important for fetching complete data; **/
			$this->pk = "ba_id";
			$this->fk = [
				[ 'pk' => 'b_id', 'model' => 'Branch', 'table' => 'branch' ]
			];
			/** Unique ID Added Random String Length **/
			$this->gidlen = 3;	
			/**  
		   		Initialize Required Post Data  
			    $this->int->struct is the DEFAULT structure and value;
			**/
  			$this->int = ( object )
  			[
  				'struct' => [
                            $this->pk => $this->generateId() ,
                            'b_id' => '',
                            'ba_clerk_pc' => '',
                            'ba_staff_pc' => '',
                            'ba_admin_pc' => '',
                            'ba_created' => Time::getTime(8)
  						 ],
                  'add' => ['b_id', 'ba_clerk_pc', 'ba_staff_pc', 'ba_admin_pc' ],
			   'update' => [ $this->pk, 'b_id', 'ba_clerk_pc', 'ba_staff_pc', 'ba_admin_pc' ],
			   'delete' => [ $this->pk ],
			   	  'get' => [ 'method', 'params' ],
				'login' => [ 'b_id', 'ba_clerk_pc', 'ba_staff_pc', 'ba_admin_pc' ],
				'all'	=> ['chunk']
  			];
		}
		
		/**
			Accepts Branch Identifier and Pincode for Account
			@b_id:  string
			@pincode: string
		**/
		function login()
		{
			if(Model::requiredPost($this->int->login))
			{
				$postData = Model::getPostData();
				
				if( $this->isAuthorized( $postData['chunk'] ) )
				{
					$account = DBLib::select($this->tableName, '*', ['b_id' => $postData['b_id']]);
					
					if($account['count'] == 1)
					{
						$accountType = null;
						
						foreach(['ba_clerk_pc', 'ba_staff_pc', 'ba_admin_pc'] as $index)
						{
							$pc = cryptoJsAesDecrypt($account['data'][$index]);
							
							if( $pc == $postData[$index] )
							{
								$accountType = $index;
							}
						}
						
						if( $accountType != null )
						{
						  $response = [
							'type' => $accountType,
							'branch_account' => $this->getById($account['data'][$this->pk])['data']
						  ];

						  $res =  json_decode(cryptoJsAesEncrypt($response), true);
							
							Encoder::success(['data' => $res ] );
						}else
						{
							Encoder::invalid();
						}
						
					}else
					{
						Encoder::invalid();
					}					
				}else
				{
					Encoder::set(['status' => 400, 'message' => 'You are not authorized!']);
				}
			}else
			{
				Encoder::mpd();
			}
		}
		function all()
		{
			if( Model::requiredPost( $this->int->all ) )
			{
				$postData = Model::getPostData();
				
				if($this->isAuthorized( $postData['chunk'] ))
				{
					$this->printAllData();					
				}else
				{
					Encoder::set(['status' => 400, 'message'=> 'You are not authorized!']);
				}

			}else
			{
				Encoder::invalid();
			}
		}
		
	}
?>
