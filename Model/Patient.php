<?php

	class Patient extends _init
    {
		function __construct()
		{
			//Initialize Object;
			$this->init();
		}
		function init()
		{
			$this->tableName = "tbl_patient";
			/** This variable is used in generating Unique ID for these Class **/
			$this->prefix = "P";
			/** These variables are important for fetching complete data; **/
			$this->pk = "p_id";
			$this->fk = [];
			/** Unique ID Added Random String Length **/
			$this->gidlen = 3;	
					   /**  
		   		Initialize Required Post Data  
			    $this->int->struct is the DEFAULT value every time ADD is called
			**/
			$this->idGenerator = 'patientId';
  			$this->int = ( object )
  			[
  				'struct' => [
                            $this->pk => $this->generateId() ,
                            'p_fname' => '',
                            'p_mname' => '',
                            'p_lname' => '',
                            'p_bdate' => Time::getTime(8),
							'p_street' => false,
							'p_barangay' => '',
							'p_city_mun' => '',
							'p_province' => '',
							'p_contact' => '',
							'p_religion' => '',
							'p_nationality' => '',
							'p_hid' => false
  						 ],
                  'add' => ['p_fname', 'p_mname', 'p_lname', 'p_bdate', 
							'p_street', 'p_barangay', 'p_city_mun', 'p_province', 
							'p_contact', 'p_religion', 'p_nationality' 
						   ],
			   'update' => [ $this->pk, 'p_fname', 'p_mname', 'p_lname', 'p_bdate', 
						   	'p_street', 'p_barangay', 'p_city_mun', 'p_province', 
							'p_contact', 'p_religion', 'p_nationality' , 'p_hid'
						   ],
			   'delete' => [ $this->pk ] ,
			   'get'	=> [ 'method', 'params' ],
			   'all'	=> [ 'chunk' ]
  			];
		}
		function patientId($object)
		{
			$patientRecord = Model::object( 'PatientRecord' );

			$time = Time::chunk( $object['date'] );
			$branchPatients = $patientRecord->recordCountByBranchAndDate( $object['b_id'],$object['date'] );
			$prefix = implode('-', [ $time->year . $time->month, $object['b_id'], $this->digit($branchPatients + 1) ]);

			return $prefix;
		}
		function digit( $root )
		{
			if( strlen($root) < 3 )
			{
				$append = '';
				for( $i = strlen($root); $i < 3; $i++ )
				{
					$append .= "0";
				}
				
				return $append.$root;
			}
		}
		function all()
		{
			if( Model::requiredPost( $this->int->all ) )
			{
				$postData = Model::getPostData();
				
				if($this->isAuthorized( $postData['chunk'] ))
				{
					$this->printAllData();					
				}else
				{
					Encoder::set(['status' => 400, 'message'=> 'You are not authorized!']);
				}

			}else
			{
				Encoder::invalid();
			}
		}
		function removePatient($params)
		{	
			if( isset($params['p_id']) )
			{
				$patient_id = $params['p_id'];
				$this->data = [$this->pk => $patient_id];
				
				$patientRecord = Model::object( 'PatientRecord' );
				
				if( $this->deleteData())
				{
					//Delete Patient Record;
					$prData = DBLib::select('tbl_patient_record', '*', [$this->pk => $patient_id]);
					

					if( $prData['status'] == 200 )
					{
						$patientRecord->setData(['pr_id' => $prData['data']['pr_id']]);

						$deletingData = DBLib::execute("DELETE FROM tbl_patient_record WHERE pr_id=:pr_id", ['pr_id' => $prData['data']['pr_id']]);
						if( $deletingData['status'] == 200 )
						{
							$file = Model::object('File');
							$patientData = $prData['data'];

							if($file->removePatientFiles($patientData['b_id'], $patient_id))
							{
								Encoder::set(['status' => 200, 'message' => 'Deleting data was successful.']);
							}else
							{
								Encoder::set(['status' => 200, 'message' => 'Deleting files was unsuccessful.']);
							}
							
						}else
						{
							Encoder::set(['status' => 400, 'message' => 'Error Deleting Data..']);
						}
					}else
					{
						Encoder::set(['status' => 400, 'message' => 'Error Deleting Data...']);
					}
				}else
				{
					Encoder::set(['status' => 400, 'message' => 'Error Deleting Data.']);
				}
			}
			
		}
	}
?>
