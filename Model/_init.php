<?php

    class _init
    {
		/** This variable is used for CRUD Methods **/
		protected $data;
		/** This is important in locating DB Table using DBLIB  **/
		protected $tableName;
		/** This variable is used in generating Unique ID for these Class **/
		protected $prefix;
		/** These variables are important for fetching complete data; **/
		protected $pk;
		protected $fk = [];
		/** Unique ID Added Random String Length **/
		protected $gidlen;
		protected $int;
		protected $callObject;
		protected $dblib_response;
		protected $authorized = false;
		
		protected $before_action;
		protected $after_action;
		
		protected $idGenerator;
		
		protected $after_saving;
		protected $postData;
		//Accepts Object
		function __construct(){}
		
        function __call($name, $args)
        {
			$this->checkUri( $name );
        }
		function setData($data)
		{
			$this->data = $data;
		}
		function property()
		{
			Encoder::set( Settings::checkArray(DBLib::tableProperties( $this->tableName )) );
		}
		function checkUri( $name )
		{
			$object = $this->isKey( $name );
			if( $object['isKey'] )
			{
				Encoder::set( $object['data'] );
			}else
			{
				Encoder::set(['status' => 400, 'message' => "No such '$name' method exists."]);
			}
		}
		function generateId()
		{
			$chunk = [ $this->prefix, Time::implodedTime(), 
					  strtoupper(CryptoLib::randomString($this->gidlen)) 
					 ];
			return implode( '-', $chunk );
		}
		function printAllData()
		{
			if($this->authorized)
			{
				$dataIndex = 'data';
				$data = Settings::checkArray( DBLib::selectAll( $this->tableName ) );
				if( sizeof( $this->fk ) > 0 )
				{
					$newData = [];
					foreach( $data[$dataIndex] as $row )
					{
						array_push( $newData, $this->getById( $row[ $this->pk ] )['data'] );
					}
					$data[$dataIndex] = $newData;
				}

				Encoder::set( $data );				
			}else
			{
				Encoder::invalid();
			}
		}
 		function add()
		{
			if( Model::requiredPost( $this->int->add ) )
			{
				$this->postData = Model::getPostData();
				$this->data = $this->struct( $this->int->add, $this->postData );
				
				if( $this->isAuthorized($this->postData['chunk']) )
				{
					if( isset( $this->idGenerator ) )
					{
						$this->data[$this->pk] = $this->{ $this->idGenerator }($this->postData);	
					}
					
					if( $this->saveNewData() )
					{
						
						Encoder::set( ['status' => 200, 'message' => 'Saving data was successful.', 'data' => $this->data ] );

					}else
					{
						Encoder::set( ['status' => 400, 'message' => 'Saving data was unsuccessful.'] );
					}					
				}else
				{
					Encoder::invalid();
				}
				$this->callback();
			}else
			{
				Encoder::set([ 'status' => 400, 'message' => 'Missing Post Data' ]);
			}
		}

		function update()
		{
			if( Model::requiredPost( $this->int->update ) )
			{
				$this->postData = Model::getPostData();
				$this->data = $this->setRequiredData( $this->int->update,  $this->postData );

				if( $this->isAuthorized( $this->postData['chunk'] ) )
				{
					if( $this->updateData() )
					{
						Encoder::set([ 'status' => 200, 'message' => 'Data was successfully updated.', 'data' => $this->data ]);

					}else
					{
						Encoder::set([ 'status' => 400, 'message' => 'Error updating data.' ]);
					}					
				}else
				{
					Encoder::invalid();
				}
				$this->callback();
			}else
			{
				Encoder::set( ['status' => 400, 'message' => 'Missing Post Data'] );
			}
		}

		function delete()
		{
			if( Model::requiredPost( $this->int->delete ) )
			{
				$this->postData = Model::getPostData();
				$this->data = $this->setRequiredData( $this->int->delete, $this->postData );

				if( $this->isAuthorized( $this->postData['chunk'] ) )
				{
					if( $this->deleteData() )
					{
						Encoder::set( ['status' => 200, 'message' => 'Data was successfully deleted.'] );
					}else
					{
						Encoder::set( ['status' => 400, 'message' => 'Error Deleting data.'] );
					}					
				}else
				{
					Encoder::invalid();
				}
				$this->callback();
			}else
			{
				Encoder::set(['status' => 400, 'message' => 'Missing Post Data']);
			}
		}
		
		function setRequiredData( $required, $post )
		{
			$result = [];
			foreach( $required as $index )
			{
				$result[$index] = $post[$index];
			}
			return $result;
		}
		
		function struct( $object, $data )
		{
			extract( $data );
			foreach( $object as $index )
			{
				$this->int->struct[ $index ] = ${ $index };
			}

			return $this->int->struct;
		}

		function isKey( $id )
		{
			$models = [ $this->pk ];
			foreach( $this->fk as $fk ){ array_push( $models, $fk['pk'] ); }
			foreach( $models as $model )
			{
				/** Check if KEY of this CLASS **/
				$data = DBLib::select( $this->tableName, '*', [ $model => $id ] );
				$data = Settings::checkArray( $data );
				if( $data['count'] > 0 )
				{
					$res = [];
					$response = $data['data'];
					foreach( $response as $row )
					{
						array_push( $res, $this->getById( $row[ $this->pk ] )['data'] );
					}
					$data['data'] = ( sizeof( $res ) == 1 ) ? $res[0] : $res;
					return ['isKey' => true, 'data' => ( $model == $this->pk ) ? $data : Settings::checkArray( $data ) ];
				}
			}
			return ['isKey' => false];
		}
		function isId( $id )
		{
			$data = DBLib::select( $this->tableName, '*', [ $this->pk => $id ] );
			return ( $data['count'] > 0 ) ?  true : false;
		}
		function getProp()
		{
			return [ 'pk' => $this->pk , 'table' => $this->tableName ];
		}
		function getFk( $model, $pk )
		{
			$tempModel = Model::object( $model );
			return $tempModel->getById( $pk );
		}

		function getAll()
		{
			return Settings::checkArray( DBLib::selectAll( $this->tableName ) );
		}
		function getById( $id )
		{
			$this->data[$this->pk] = $id;
			$fk = $this->fk;

			$data = DBLib::select( $this->tableName, '*', [ $this->pk => $id ] );

			if( isset($data['data']) )
			{
				foreach( $data['data'] as $key => $val )
				{
					foreach( $fk as $row )
					{
						if( $key == $row['pk'] )
						{
							$v = $this->getFk( $row['model'], $val );
							unset( $data['data'][ $key ] );
							$data['data'][ $row['table'] ] = (isset($v['data'])) ? $v['data'] : "";
						}
					}
				}
				return $this->injectData( $data );				
			}
			return "";
		}
		function saveNewData()
		{
			if( $this->authorized )
			{
				$dblib_response = DBLib::insert( $this->tableName, $this->data );
				$this->dblib_response = $dblib_response;
                
                $this->afterAction();
				return ( $dblib_response['status'] == 200 )? true : false;				
			}else
			{
				Encoder::invalid();
			}
		}
		function updateData()
		{
			if( $this->authorized )
			{
				$data = $this->data;
				$pointer = [ $this->pk => $this->data[$this->pk] ];
				unset( $data[$this->pk] );
				$update  = $data;
				$dblib_response = DBLib::update( $this->tableName, $update, $pointer );

				

				return ( $dblib_response['status'] == 200 ) ? true : false;				
			}else
			{
				Encoder::invalid();
			}
		}
		function deleteData()
		{
			if( $this->authorized )
			{
				if( isset( $this->data[$this->pk] ) )
				{
					$dblib_response = DBLib::execute("DELETE FROM $this->tableName WHERE $this->pk = :$this->pk", $this->data );

					return ( $dblib_response['status'] == 200 ) ? true : false;
				}else
				{
					return false;
				}				
			}else
			{
				Encoder::invalid();
			}
		}
		function injectData( $response )
		{
			return $response;
		}
		/** 
			Post Data Required
			@method
			@params
		**/
		function get()
		{
			if( Model::requiredPost( $this->int->get ) )
			{
				$postData = Model::getPostData();
				if( $this->isAuthorized($postData['chunk']) )
				{
					$this->data = $this->setRequiredData( $this->int->get, $postData );
					$this->data['params'] = json_decode( $this->data['params'], true );
					$this->{ $this->data['method'] }( $this->data['params'] );					
				}else
				{
					Encoder::invalid();
				}
			}else
			{
				Encoder::set(['status' => 400, 'message' => 'Missing Post Data']);
			}	
		}
		function callback()
		{
			if( isset($this->callObject) )
			{
				if( isset( $this->callObject['call'] ) )
				{
					if( isset( $this->callObject['args'] ) )
					{
						$this->{ $this->callObject['call'] }( $this->callObject['args'] );			
					}else
					{
						$this->{ $this->callObject['call'] }();
					}
				}
			}
		}
		function isAuthorized($key)
		{
			$separator = Settings::getSeparator();
			$ini_conf = parse_ini_file(implode( $separator, [ 'Resource', 'ss', 'ifhti.ini' ] ));
			
			$key = implode('+', explode(' ', $key ));
			
			$this->authorized = (  $ini_conf['key'] === cryptoJsAesDecrypt( $key ) );
		

			return $this->authorized;
		}
		function afterAction()
		{
			if(isset($this->after_action))
			{
				if( is_array($this->after_action))
				{
					if(isset($this->after_action['call']))
					{
						if(isset($this->after_action['args']))
						{
							$this->{ $this->after_action['call'] }( $this->after_action['args'] );			
						}else
						{
							$this->{ $this->after_action['call'] }();			
						}
					}					
				}else
				{
					$this->{ $this->after_action }();
				}
			}
		}
	}



?>
