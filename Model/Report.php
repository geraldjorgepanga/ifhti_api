<?php

	class Report extends _init
    {
		function __construct()
		{
			//Initialize Object;
			$this->init();
		}
		function init()
		{
			$this->tableName = "tbl_report";
			/** This variable is used in generating Unique ID for these Class **/
			$this->prefix = "RPT";
			/** These variables are important for fetching complete data; **/
			$this->pk = "r_id";
			$this->fk = [
				[ 'pk' => 'b_id', 'model' => 'Branch', 'table' => 'branch' ]
			];
			/** Unique ID Added Random String Length **/
			$this->gidlen = 3;	
					   /**  
		   		Initialize Required Post Data  
			    $this->int->struct is the DEFAULT value every time ADD is called
			**/
  			$this->int = ( object )
  			[
  				'struct' => [
                            $this->pk => $this->generateId() ,
                            'r_filename' => '',
                            'r_created' => Time::getTime(8)
  						 ],

                  'add' => ['r_filename' ],
			   'update' => [ $this->pk, 'r_filename' ],
			   'delete' => [ $this->pk ] ,
			   'get'	=> [ 'method', 'params' ],
			   'required'=> [ 'chunk' ]
  			];
			
			$this->after_action = 'debugMode';
		}
		function debugMode()
		{
//			Encoder::set( $this->dblib_response );
		}
		function required()
		{
			if( Model::requiredPost( $this->int->required ) )
			{
				$data = DBLib::toArray( DBLib::selectAll($this->tableName) );

				Encoder::success(['data' => $data['data'] ]);
			}else
			{
				Encoder::invalid();
			}
		}
		function createReportFolder($date)
		{
			$required = [ 'Data', 'Clerk', 'Reports', $date];

			$append = '';
			$success = true;
			foreach( $required as $directory )
			{
				$append .= Settings::getSeparator() . $directory;
				if( !file_exists(  getcwd() . $append ) )
				{
					$success = ( mkdir( getcwd() . $append )  ) ? $success : false;
				}			
				
			}
			
			return $success;
		}
		function directory_list($args)
		{

			if( isset( $args['date'] ) && isset( $args['b_id'] ) )
			{
				$dirs = [ 'Data', 'Reports', $args['b_id'], $args['date']  ];
				$file = implode( Settings::getSeparator(), $dirs );
				$append = getcwd();

				foreach( $dirs as $dir )
				{
					$append .= Settings::getSeparator() . $dir;
					if( !file_exists( $append ) )
					{
						mkdir( $append );
					}
				}
				Encoder::success(['data' => scandir( $file )]);
			}else
			{
				Encoder::invalid();
			}
			
		}
		
	}
?>
