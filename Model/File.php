<?php

class File extends _init
{
	protected $dir;
	protected $patients = ['Data', 'Patients'];
	function __construct()
	{
		//Initialize Object;
		$this->init();
	}
	function init()
	{
		$this->int = ( object )
		[
			  'get' => [ 'method', 'params' ]
		];
	}
	/**
		From Root Directory
		params: @filename: [string]
	**/ 
	function setDirectory( $path )
	{
		$this->dir = implode( $this->getSeparator(), $path );
	}
	function getSeparator()
	{
		$separator = '/';
		$cwd = explode( $separator , getcwd() );
			
		return ( sizeof($cwd) > 1 ) ? $separator : '\\';
	}
	function createDirectory($path)
	{
		$dir = getcwd();
		$slash = Settings::getSeparator();
		foreach( $path as $pathname )
		{
			$dir .= $slash . $pathname;
			
			if(!file_exists($dir))
			{
				mkdir($dir);
			}
		}
		
		return;
	}
	function add()
	{
		$this->createDirectory(['sample']);
	}
	/**
		Accepts 
		@directory [string]
	**/
	function removeDirectory($directory)
	{

		$_root = getcwd();
		$slash = Settings::getSeparator();

		$_dir =  $_root . $slash . Setting::implode( $slash, $directory);

		if(file_exists($_dir))
		{
			return (rmdir($_dir));			
		}else
		{
			return true;
		}
	}
	function removePatientFiles($b_id, $p_id)
	{
		if( isset($p_id) && isset($b_id) )
		{
			$slash = Settings::getSeparator();
			$_dir = getcwd() . $slash . implode( $slash , array_merge( $this->patients, [ $b_id, $p_id ] ) );
			
			if(file_exists($_dir))
			{
				return (rmdir($_dir));			
			}else
			{
				return true;
			}
		}else
		{
			return false;	
		}
	}
	function createPatientFile($b_id, $p_id)
	{
		if( isset($b_id) && isset($p_id) )
		{
			return $this->createDirectory(array_merge( $this->patients, [ $b_id, $p_id ]));
		}else
		{
			return false;
		}
	}
	function renameFile($object)
	{
		if(isset($object['old']) && isset($object['new']))
		{
			$root = [getcwd()];

			$old_dir = array_merge( $root, $object['old'] );
			$new_dir = array_merge( $root, $object['new'] );

			$slash = Settings::getSeparator();

			if( rename( implode($slash, $old_dir), implode( $slash, $new_dir ) ) )
			{
				Encoder::set(['status' => 200, 'message' => 'Success renaming file.']);
			}else
			{
				Encoder::set(['status' => 400, 'message' => 'Error renaming file.']);
			}
			
		}else
		{
			Encoder::mpd();
		}
	}
	function openDirectory($object)
	{
			if(isset($object))
			{
				$root = [getcwd()];
				$dir = $object['dir'];
				$_dir = array_merge( $root, $dir );

				$toCheck = getcwd();
				foreach($dir as $dir_)
				{
					$toCheck .= Settings::getSeparator() . $dir_;
					if(!file_exists( $toCheck ))
					{
						mkdir( $toCheck );
					}
				}

				$res = scandir(implode(Settings::getSeparator(),$_dir ));
				$_res = [];		   
			   foreach($res as $li)
			   {	
				   if($li != ".")
				   {
					   array_push($_res, [ 'filename' => $li, 'filetype' => filetype( $toCheck . Settings::getSeparator() . $li )]);
				   }
			   }
				Encoder::set([
					'status' => 200,
					'message'=> 'Fetched.',
					'data' => $_res
				]);

			}else
			{
				Encoder::set([
					'status' => 400,
					'message'=> 'No Directory found.',
					'data' => []
				]);

			}
	}
	function deleteFile($object)
	{
		if(isset($object['dir']))
		{
			$root = [getcwd()];
			$dir = array_merge( $root, $object['dir'] );

			$slash = Settings::getSeparator();

			if( unlink( implode($slash, $dir) ) )
			{
				Encoder::set(['status' => 200, 'message' => 'Success deleting file.']);
			}else
			{
				Encoder::set(['status' => 400, 'message' => 'Error deleting file.']);
			}
			
		}else
		{
			Encoder::mpd();
		}		
	}
	function deleteDirectory($object)
	{
		if(isset($object['dir']))
		{
			$root = [getcwd()];
			$dir = array_merge( $root, $object['dir'] );

			$slash = Settings::getSeparator();

			if( rmdir( implode($slash, $dir) ) )
			{
				Encoder::set(['status' => 200, 'message' => 'Success deleting directory.']);
			}else
			{
				Encoder::set(['status' => 400, 'message' => 'Error deleting directory.']);
			}
			
		}else
		{
			Encoder::mpd();
		}			
	}
	function newFolder($object)
	{
		if(isset($object['dir']) && isset( $object['filename'] ))
		{
			$root = [getcwd()];
			$slash = Settings::getSeparator();
			$dir = implode( $slash, array_merge( $root, $object['dir'] ) );
			
			if( file_exists( $dir . $slash . $object['filename'] ) )
			{
				Encoder::set(['status' => 400, 'message' => 'File already exists.']);
			}else
			{
				if(mkdir( $dir . $slash . $object['filename'] ))
				{
					Encoder::set(['status' => 200, 'message' => 'Success creating folder']);
				}else
				{
					Encoder::set(['status' => 400, 'message' => 'Error creating folder']);
				}
			}
		}else
		{
			Encoder::mpd();
		}		
	}
	/**
		Object

		[ Data, Patients, 001, 2018-05-04 ]
	**/
	function createPatientServiceRecord($object)
	{
		$root = getcwd();		
		$slash = Settings::getSeparator();
		$error = 0;
		foreach($object as $dir)
		{
			$root .= $slash . $dir;
			
			if(!file_exists( $root ))
			{
				$error = (mkdir($root)) ? $error : 1;
			}
		}
		return $error;
	}
}


?>