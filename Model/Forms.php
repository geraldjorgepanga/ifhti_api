<?php

	class Forms extends _init
    {
		function __construct()
		{
			//Initialize Object;
			$this->init();
		}
		function init()
		{
			$this->tableName = "tbl_forms";
			/** This variable is used in generating Unique ID for these Class **/
			$this->prefix = "F";
			/** These variables are important for fetching complete data; **/
			$this->pk = "f_id";
			$this->fk = [];
			/** Unique ID Added Random String Length **/
			$this->gidlen = 3;	
			/**  
		   		Initialize Required Post Data  
			    $this->int->struct is the DEFAULT value every time ADD is called
			**/
  			$this->int = ( object )
  			[
  				'struct' => [
                            $this->pk => $this->generateId() ,
                            'b_id' => '',
                            'news_title' => '',
                            'news_content' => '',
                            'news_file' => '',
                            'news_posted' => Time::getTime(8)
  						 ],

			   'add' => ['b_id', 'news_title', 'news_content', 'news_posted', 'news_file' ],
			   'update' => [ $this->pk, 'b_id', 'news_title', 'news_content', 'news_file' ],
			   'delete' => [ $this->pk ] ,
			   'get'	=> [ 'method', 'params' ],
			   'all'	=> ['chunk']
  			];
        }

        function methods()
        {
            $class   = new ReflectionClass($this);
            $methods = $class->getMethods();
            $except = [
                '__construct',
                'init',
                'methods'
            ];
            $methodList = [];

            foreach($methods as $method)
            {
                if( $method->class == 'Forms' )
                {
                    if(!in_array( $method->name, $except ))
                    {
                        $split = explode( '_', $method->name );
                        $value = [];
                        if( in_array('data', $split) )
                        {
                            $value = $this->{ $method->name }(false);
                        }
                        array_push( $methodList, [ 'name' => $method->name, 'class' => $method->class, 'data' => $value ] );
                    }
                }
            }

            Encoder::set( ['status' => 200, 'message' => 'Success', 'data' => $methodList] );
        }

        /**
         Face Sheet PDF Generator.
         
         With parameter : Form with Details will be generated;
         W/O parameter: Blank form will be generated;

         Able to accept parameter as array;
         Array
         [ 
             'case_no' => string,
             'p_surname' => string,
             'p_firstname' => string,
             'p_middlename => string,
             'p_age' => int,
             'p_birthdate' => string,
             'p_birthplace' => string,
             'p_contact' => string,
             'p_address' => string,
             'p_civilstatus' => string,
             'p_religion' => string,
             'p_nationality' => string,
             'p_occupation' => string,
             'next_to_kin' => string,
             'ntk_relationship' => string,
             'ntk_address' => string,
             'p_date_admitted' => string,
             'p_time_admitted' => string,
             'p_admitting_diagnosis' => string,
             'p_admitting_physician' => string,
             'p_date_discharged' => string,
             'p_time_discharged' => string,
             'p_final_diagnosed' => string,
             'p_condition_upon_discharged' => string

         ]
        **/ 
        function face_sheet()
        {
            if(isset($_POST['path']))
            {
                
                $path = json_decode($_POST['path'], true);

                $file_title = $path[ sizeof($path) - 1 ];
                $filename   = $path[ sizeof($path) - 1 ] . ".pdf";

                $data = $this->face_sheet_data( (func_num_args() > 0) ? func_get_arg(0) : false );
                $pdf = new FPDF('P','mm',array(100,150));       
                $pdf->AddPage("P");    
                $pdf->SetMargins(1.5, 1, 1.5);
                $pdf->SetTitle($file_title);

                $pdf->SetLineWidth(0.1);
                $pageW = $pdf->GetPageWidth();
                $pdf->Ln();
                $slash = Settings::getSeparator();
                $logo_dir = implode( $slash, [ getcwd(), 'Resource', 'img', 'logo.jpg' ] );
                $pdf->Image( $logo_dir ,10,5,-300);
                $pdf->Ln();

                $pdf->SetXY(40, 16);
                //Print Title
                $pdf->SetFont('Arial','B', 8); 
                $pdf->Write(5, "FACE SHEET");

                $pdf->Ln();
                //Print Title
                $pdf->SetFont('Arial','B', 7);
                $pdf->Cell( 20 ,5,'Case Number: ',"",0,'L', 0);

                $pdf->SetFont('Courier','', 6);
                $pdf->Cell( 30 ,4, $data['case_no'] ,"B",0,'L', 0);            
                $pdf->Ln();
                $pdf->Ln();

                // Set font
                $tableXY = [ 'x' => $pdf->GetX(), 'y' => $pdf->GetY() ];

                $pdf->SetFont('Arial','B',6);
                $pdf->Cell( 45 ,5,'Surname         First Name       Middle Name',"LTR",0,'L', 0);
                $pdf->Cell( 9 ,5,"Age" ,"LTR",0,'C', 0);
                $pdf->Cell( 10 ,5,"DOB" ,"LTR",0,'C', 0);
                $pdf->Cell( 17 ,5,"POB" ,"LTR",0,'C', 0);
                $pdf->Cell( 15 ,5,"Contact No" ,"LTR",0,'C', 0);
                $pdf->Ln();

                $x = $pdf->GetX();
                $y = $pdf->GetY();

                $pdf->SetFont('Courier','',5);
                $pdf->SetY($y);
                $pdf->SetX($x); $x += 45;

                $fullName = ( $data['p_surname'] == '' ) ? "" : $data['p_surname'] . ", " . implode(' ', [ $data['p_firstname'], $data['p_middlename'] ]);

                $pdf->MultiCell( 45 ,2, $fullName ,"LR",'L', 0);
                $pdf->SetY($y);
                $pdf->SetX($x); $x += 9;
                $pdf->MultiCell( 9 ,2, $data['p_age'] ,"LR",'C', 0);
                $pdf->SetY($y);
                $pdf->SetX($x); $x += 10;
                $pdf->MultiCell( 10 ,2, $data['p_birthdate'] ,"LR",'C', 0);
                $pdf->SetY($y);
                $pdf->SetX($x); $x += 17;
                $pdf->MultiCell( 17 ,2, $data['p_birthplace'] ,"LR",'C', 0);
                $pdf->SetY($y);
                $pdf->SetX($x); $x += 15;
                $pdf->MultiCell( 15 ,2, $data['p_contact'] ,"LR",'C', 0);
                $pdf->Ln();

                $pdf->SetFont('Arial','B',6);
                $pdf->Cell( 45 ,5,"Address" ,"LTR",0,'C', 0);
                $pdf->Cell( 9 ,5,"CS" ,"LTR",0,'C', 0);
                $pdf->Cell( 10 ,5,"Religion" ,"LTR",0,'C', 0);
                $pdf->Cell( 17 ,5,"Nationality" ,"LTR",0,'C', 0);
                $pdf->Cell( 15 ,5,"Occupation" ,"LTR",0,'C', 0);
                $pdf->Ln();

                $pdf->SetFont('Courier','',5);
                $x = $pdf->GetX();
                $y = $pdf->GetY();

                $pdf->SetY($y);
                $pdf->SetX($x); $x += 45;
                $pdf->MultiCell(45 , 2, $data['p_address'] , "LR", "C", 0);

                $pdf->SetY($y);
                $pdf->SetX($x); $x += 9;
                $pdf->MultiCell( 9 , 2, $data['p_civilstatus'] ,"LR",'C', 0);

                $pdf->SetY($y);
                $pdf->SetX($x); $x += 10;
                $pdf->MultiCell( 10 ,2 , $data['p_religion'] ,"LR",'C', 0);

                $pdf->SetY($y);
                $pdf->SetX($x); $x += 17;
                $pdf->MultiCell( 17 ,2, $data['p_nationality'] ,"LR",'C', 0);

                $pdf->SetY($y);
                $pdf->SetX($x); $x += 15;
                $pdf->MultiCell( 15 ,2, $data['p_occupation'] ,"LR",'C', 0);
                $pdf->Ln();

                $pdf->SetFont('Arial','B',6);
                $pdf->Cell( 45 ,5,"Name of next to kin","LTR",0,'L', 0);
                $pdf->Cell( 19 ,5,"Relationships" ,"LTR",0,'C', 0);
                $pdf->Cell( 32 ,5,"Address" ,"LTR",0,'C', 0);
                $pdf->Ln();

                $pdf->SetFont('Courier','',5);
                $x = $pdf->GetX();
                $y = $pdf->GetY();

                $pdf->SetY($y);
                $pdf->SetX($x); $x+=45;
                $pdf->MultiCell( 45 , 2, $data['next_to_kin'] ,"LR",'C', 0);
                $pdf->SetY($y);
                $pdf->SetX($x); $x+=19;
                $pdf->MultiCell( 19 , 2, $data['ntk_relationship'] ,"LR",'C', 0);
                $pdf->SetY($y);
                $pdf->SetX($x); $x+=32;
                $pdf->MultiCell( 32 , 2, $data['ntk_address'] ,"LR",'C', 0);
                $pdf->Ln();

                $pdf->SetFont('Arial','B',6);
                $pdf->Cell( 45 ,5,"Date admitted","LTR",0,'L', 0);
                $pdf->Cell( 51 ,5,"Time admitted" ,"LTR",0,'L', 0);
                $pdf->Ln();

                $pdf->SetFont('Courier','',5);
                $pdf->Cell( 45 ,8, $data['p_date_admitted'] ,"LBR",0,'C', 0);
                $pdf->Cell( 51 ,8, $data['p_time_admitted'] ,"LBR",0,'C', 0);
                $pdf->Ln();

                $pdf->SetFont('Arial','B',6);
                $pdf->Cell( 45 ,5,"Admitting Diagnosis","LTR",0,'L', 0);
                $pdf->Cell( 51 ,5,"Admitting Physician" ,"LTR",0,'L', 0);
                $pdf->Ln();

                $pdf->SetFont('Courier','',5);
                $x = $pdf->GetX();
                $y = $pdf->GetY();

                $pdf->SetY($y);
                $pdf->SetX($x); $x+=45;
                $pdf->MultiCell(45 , 2, $data['p_admitting_diagnosis'] , "LR", "L", 0);
                $pdf->SetY($y);
                $pdf->SetX($x); $x+=51;
                $pdf->MultiCell( 51 , 3, $data['p_admitting_physician'] ,"LR",'C', 0);
                $pdf->Ln();

                $pdf->SetY( $pdf->GetY() );
                $pdf->SetX( $pdf->GetX() );
                $pdf->SetFont('Arial','B',6);
                $pdf->Cell( 45 ,5,"Date Discharged","LTR",0,'L', 0);
                $pdf->Cell( 51 ,5,"Time Discharged" ,"LTR",0,'L', 0);
                $pdf->Ln();

                $pdf->SetFont('Courier','',5);
                $pdf->Cell( 45 ,10, $data['p_date_discharged'] ,"LBR",0,'C', 0);
                $pdf->Cell( 51 ,10, $data['p_time_discharged'] ,"LBR",0,'C', 0);
                $pdf->Ln();

                $pdf->SetFont('Arial','B',6);
                $pdf->Cell( 45 ,5,"Final Diagnosis","LTR",0,'L', 0);
                $pdf->Cell( 51 ,5,"Condition Upon Discharged" ,"LTR",0,'L', 0);
                $pdf->Ln();

                $pdf->SetFont('Arial','B',6);
                $x = $pdf->GetX();
                $y = $pdf->GetY();

                $condition = $data['p_condition_upon_discharged'];

                $pdf->SetY($y);
                $pdf->SetX($x); $x+=45;
                $pdf->MultiCell( 45 ,8, $data['p_final_diagnosis'] ,"LR",'C', 0);
                $pdf->SetY($y);
                $pdf->SetX($x); $x+=51;

                $conditionText = [ 
                    "( ".$condition['recovered']." ) Recovered   ( ".$condition['died']." ) Died",
                    "( ".$condition['discharged']." ) Discharged  ( ".$condition['transferred']." ) Transferred",
                    "( ".$condition['hama']." ) HAMA           ( ".$condition['home_per_request']." ) Home Per Request"
                ];
                $pdf->MultiCell( 51 ,2, implode("\n", $conditionText ) ,"LR",'L', 0);
                $pdf->Ln();
                $pdf->MultiCell( 96 ,8, "","T",'C', 0);

                $pdf->Rect( $tableXY['x'] , $tableXY['y'], 96, 110 , "D");
                //Output File
                
                $path = implode(Settings::getSeparator(), $path ) . ".pdf";
                $pdf->Output("F", $path );
                
                Encoder::set(['status' => 200, 'message' => 'File successfully generated.']);
            }else
            {
                Encoder::mpd();
            }
            
        }
        function face_sheet_data($data)
        {
            if(!$data)
            {
                return [
                    'case_no' => '',
                    'p_surname' => '',
                    'p_firstname' => '',
                    'p_middlename' => '',
                    'p_age' => '',
                    'p_birthdate' => '',
                    'p_birthplace' => '',
                    'p_contact' => '',
                    'p_address' => '',
                    'p_civilstatus' => '',
                    'p_religion' => '',
                    'p_nationality' => '',
                    'p_occupation' => '',
                    'next_to_kin' => '',
                    'ntk_relationship' => '',
                    'ntk_address' => '',
                    'p_date_admitted' => '',
                    'p_time_admitted' => '',
                    'p_admitting_diagnosis' => "",
                    'p_admitting_physician' => '',
                    'p_date_discharged' => '',
                    'p_time_discharged' => '',
                    'p_final_diagnosis' => '',
                    'p_condition_upon_discharged' => [ 
                        'recovered' => '  ',
                        'discharged' => '  ',
                        'hama' => '  ',
                        'died' => '  ',
                        'transferred' => '  ',
                        'home_per_request' => '  ' 
                    ]
                ];
            }

            return $data;
        }
                /**
         Admission Letter PDF Generator.
         
         With parameter : Form with Details will be generated;
         W/O parameter: Blank form will be generated;

         Able to accept parameter as array;
         Array
         [ 
             'case_no' => string,
             'date' => string,
             'time' => string,
         ]
        **/ 
        function admission_letter()
        {
            if(isset($_POST['path']))
            {
                
                $path = json_decode($_POST['path'], true);

                $file_title = $path[ sizeof($path) - 1 ];
                $filename   = $path[ sizeof($path) - 1 ] . ".pdf";

                $data = (func_num_args() > 0) ? func_get_arg(0) : false;
                $data = $this->admission_letter_data( $data );

                $pdf = new FPDF('P','mm',array(100,150));       
                $pdf->AddPage("P");    
                $pdf->SetMargins(1.5, 1, 1.5);
                $pdf->SetTitle($file_title);
                $pdf->SetLineWidth(0.1);
                $pageW = $pdf->GetPageWidth();
                $pdf->Ln();

                $slash = Settings::getSeparator();
                $logo_dir = implode( $slash, [ getcwd(), 'Resource', 'img', 'logo.jpg' ] );
                $pdf->Image( $logo_dir ,10,5,-300);
                $pdf->Ln();

                $pdf->SetFont( "Arial", "", 4);
                $pdf->SetXY( 1.5, 25 );

                $pdf->Cell(15, 2, "Admission Case No:", 0, 0, "L", 0, 0);
                //Prints admission case no. here
                $pdf->SetFont( "Courier", "", 4);
                $pdf->Cell(20, 2, $data['case_no'] , "B", 1, "C", 0, 0);

                $pdf->SetFont( "Arial", "", 4);            
                $pdf->Cell(15, 2, "Date:", 0, 0, "L", 0, 0);

                $pdf->SetFont( "Courier", "", 4);            
                $pdf->Cell(20, 2, $data['date'] , "B", 1, "C", 0, 0);

                $pdf->SetFont( "Arial", "", 4);                        
                $pdf->Cell(15, 2, "Time:", 0, 0, "L", 0, 0);

                $pdf->SetFont( "Courier", "", 4);
                $pdf->Cell(20, 2, $data['time'] , "B", 1, "C", 0, 0);
                $pdf->Ln();

                $pdf->SetXY( 1.5, 40 );
                $pdf->SetFont( "Arial", "", 7);
                $pdf->Write( 4, "                   Ako si _____________________________________________ nakatira sa _________________________________________________________________ nasa hustong gulang at pag-iisip ay kusang loob na tumungo sa", 0);
                $pdf->SetFont( "Arial", "B", 7);
                $pdf->Write( 4, " ISAROG FAMILY HEALTH AND TRAINING, INC. - BIRTHING CLINIC/PAANAKAN ", 0);

                $pdf->SetFont( "Arial", "", 7 );
                $pdf->Write( 4,"upang dito ay tumanggap ng serbisyong panganganak. Aking tatanggapin ang anumang proceso na may kinalaman sa aking ", 0);

                $pdf->SetFont( "Arial", "B", 7);
                $pdf->Write( 4, "NORMAL NA PANGANGANAK. ", 0 );

                $pdf->SetFont( "Arial", "", 7 );
                $pdf->Write( 4, "Kung sakali man na may hindi inaasahang pangyayari sa panahong iyon, ako ay pumapayag na ilipat sa mas malaking ospital at babayaran ang mga nagamit sa pasilidad, at tatalikuran ko ang anumang reklamo ukol sa bagay na ito.", 0 );

                $pdf->Ln();
                $pdf->SetXY( 1.5, 100 );

                $pdf->Cell( 48.5 , 3, "_________________________", "", 0, "C", 0, 0);
                $pdf->Cell( 48.5 , 3, "_________________________", "", 1, "C", 0, 0);

                $pdf->SetFont( "Arial", "B", 5 );
                $pdf->Cell( 48.5 , 3, "Lagda ng Pasyente", "", 0, "C", 0, 0);
                $pdf->Cell( 48.5 , 3, "Lagda ng Saksi", "", 1, "C", 0, 0);
                $pdf->Cell( 48.5 , 3, "(Signature over printed name)", "", 0, "C", 0, 0);
                $pdf->Cell( 48.5 , 3, "(Signature over printed name)", "", 1, "C", 0, 0);            

                $path = implode(Settings::getSeparator(), $path ) . ".pdf";


                $pdf->Output("F", $path );
                
                Encoder::set(['status' => 200, 'message' => 'File successfully generated.']);
            }else
            {
                Encoder::mpd();
            }
        }
        function admission_letter_data($data)
        {
            return (!$data) ? [
                'case_no' => '',
                'date' => '',
                'time' => ''
            ] : $data;
            
        }

        function admission_note()
        {
            if(isset($_POST['path']))
            {
                
                $path = json_decode($_POST['path'], true);

                $file_title = $path[ sizeof($path) - 1 ];
                $filename   = $path[ sizeof($path) - 1 ] . ".pdf";

                $data = (func_num_args() > 0) ? func_get_arg(0) : false;
                $data = $this->admission_note_data( $data );

                $pdf = new FPDF('P','mm',array(100,150));       
                $pdf->AddPage("P");    
                $pdf->SetMargins(1.5, 1, 1.5);
                $pdf->SetTitle($file_title);
                $pdf->SetLineWidth(0.1);
                $pageW = $pdf->GetPageWidth();
                $pdf->Ln();

                $slash = Settings::getSeparator();
                $logo_dir = implode( $slash, [ getcwd(), 'Resource', 'img', 'logo.jpg' ] );
                $pdf->Image( $logo_dir ,10,5,-300);
                $pdf->Ln();

                $pdf->SetFont("Arial","", 5);
                $pdf->SetXY( 45, 17 );

                $pdf->Cell(13, 2, "Case Number:", 0, 0, "L", 0);

                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(20, 2, $data['case_no'] , "B", 1, "C", 0 );
                $pdf->SetFont("Arial","", 5);            

                $pdf->Ln();

                $pdf->SetXY( 1.5, 23 );            
                $pdf->Cell(15, 2, "Name of Patient:", 0, 0, "L", 0);
                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(50, 2, $data['patient'] , "B", 0, "C", 0 );
                $pdf->SetFont("Arial","", 5);            

                $pdf->Cell(6, 2, "Age:", 0, 0, "L", 0);

                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(20, 2, $data['age'] , "B", 1, "C", 0 );

                $pdf->SetXY( 1.5, 28 );                        
                $pdf->SetFont("Arial","", 5);                       
                $pdf->Cell(15, 2, "Address:", 0, 0, "L", 0);
                $pdf->SetFont("Courier","", 5);                        
                $pdf->Cell(76, 2, $data['address'] , "B", 0, "C", 0 );

                $pdf->Ln();
                $pdf->SetLineWidth(0.5);
                $pdf->Line( 1.5 , 35 , 97, 35);

                $pdf->Ln();

                $pdf->SetFont("Arial","", 7);
                $pdf->Ln();            
                $pdf->Cell(97, 10, "    Chief Complaint:", 0, 1, "L", 0);
                $pdf->Cell(97, 10, "    Date and Time of Admission:", 0, 1, "L", 0);
                $pdf->Cell(97, 10, "    Admitting Diagnosis:", 0, 1, "L", 0);
                $pdf->Cell(97, 10, "    Final Diagnosis:", 0, 1, "L", 0);
                $pdf->Cell(97, 10, "    Date and Time of Discharge:", 0, 1, "L", 0);
                $pdf->Cell(97, 10, "    Complications:", 0, 1, "L", 0);
                $pdf->Cell(97, 10, "    Condition upon discharge:", 0, 1, "L", 0);
                $pdf->Cell(97, 10, "    Home Medications:", 0, 1, "L", 0);
                $pdf->Cell(97, 10, "    Date of Follow up consultations:", 0, 1, "L", 0);

                $pdf->SetLineWidth(0.2);            
                $pdf->Cell(40, 5, "    ", 0, 0, "L", 0);
                $pdf->Cell(50, 5, "    ", "B", 1, "L", 0);     

                $path = implode(Settings::getSeparator(), $path ) . ".pdf";
                $pdf->Output("F", $path );
                
                Encoder::set(['status' => 200, 'message' => 'File successfully generated.']);
            }else
            {
                Encoder::mpd();
            }
            
        }
        function admission_note_data($data)
        {
            return (!$data) ? [
                'case_no' => '',
                'patient' => '',
                'age'     => '',
                'address' => ''
            ] : $data;
        }

        function doctor_order_sheet()
        {
            if(isset($_POST['path']))
            {
                
                $path = json_decode($_POST['path'], true);

                $file_title = $path[ sizeof($path) - 1 ];
                $filename   = $path[ sizeof($path) - 1 ] . ".pdf";
                $data = (func_num_args() > 0) ? func_get_arg(0) : false;
                $data = $this->doctor_order_sheet_data( $data );

                $pdf = new FPDF('P','mm',array(100,150));       
                $pdf->AddPage("P");    
                $pdf->SetMargins(1.5, 1, 1.5);
                $pdf->SetTitle($file_title);
                $pdf->SetLineWidth(0.1);
                $pageW = $pdf->GetPageWidth();
                $pdf->Ln();

                $slash = Settings::getSeparator();
                $logo_dir = implode( $slash, [ getcwd(), 'Resource', 'img', 'logo.jpg' ] );
                $pdf->Image( $logo_dir ,10,5,-300);
                $pdf->Ln();

                $pdf->SetFont("Arial","", 5);
                $pdf->SetXY( 45, 17 );

                $pdf->Cell(13, 2, "Case Number:", 0, 0, "L", 0);

                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(20, 2, $data['case_no'] , "B", 1, "C", 0 );
                $pdf->SetFont("Arial","", 5);            

                $pdf->Ln();

                $pdf->SetXY( 1.5, 23 );            
                $pdf->Cell(15, 2, "Name of Patient:", 0, 0, "L", 0);
                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(50, 2, $data['patient'] , "B", 0, "C", 0 );
                $pdf->SetFont("Arial","", 5);            

                $pdf->Cell(6, 2, "Age:", 0, 0, "L", 0);

                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(20, 2, $data['age'] , "B", 1, "C", 0 );

                $pdf->SetXY( 1.5, 28 );                        
                $pdf->SetFont("Arial","", 5);                       
                $pdf->Cell(15, 2, "Address:", 0, 0, "L", 0);
                $pdf->SetFont("Courier","", 5);                        
                $pdf->Cell(76, 2, $data['address'] , "B", 0, "C", 0 );

                $pdf->Ln();
                $pdf->SetLineWidth(0.5);
                $pdf->Line( 1.5 , 35 , 97, 35);

                $pdf->Ln();            
                $pdf->SetXY( 1.5, 38 );  
                $pdf->SetFont("Arial","B", 7);                       
                $pdf->Cell(97, 2, "DOCTOR'S ORDER SHEET", 0, 1, "C", 0);
                $pdf->Ln();

                $pdf->SetXY( 4.0, 45 );              
                $pdf->SetLineWidth(0.1);

                $pdf->SetFont("Arial","B", 5);                       
                $pdf->Cell(20 , 5, "DATE/TIME", "LTRB", 0, "C", 0);
                $pdf->Cell(70 , 5, "ORDERS", "TRB", 1, "C", 0);

                $pdf->Rect( 4.0, 50, 20, 90, "D");
                $pdf->Rect( 24, 50, 70, 90, "D");

                $pdf->SetFont("Courier","", 5);                       

                $x = 4.0; $y = 52;

                foreach($data['orders'] as $order)
                {
                    $x = 4.0;
                    $pdf->SetY($y);
                    $pdf->SetX($x); $x+=20;
                    $pdf->MultiCell( 20 , 2, $order['date'] ,0,'C', 0);

                    $orderText = $order['order'];

                    $pdf->SetY($y);
                    $pdf->SetX($x);
                    $pdf->MultiCell( 70 , 2, $orderText ,0,'L', 0);                

                    $toAdd = (sizeof(explode("\n", $orderText)) * 2) + ( $pdf->GetStringWidth( $orderText ) / 30 );
                    $y += ( $toAdd );       
                }

                $path = implode(Settings::getSeparator(), $path ) . ".pdf";
                $pdf->Output("F", $path );
                
                Encoder::set(['status' => 200, 'message' => 'File successfully generated.']);
            }else
            {
                Encoder::mpd();
            }
        }
        function doctor_order_sheet_data($data)
        {
            return (!$data) ? [
                'case_no' => '',
                'patient' => '',
                'age'     => '',
                'address' => '',
                'orders'  => [
                    ['date' => '', 'order' => ""],
                   ]
            ] : $data;
        }

        function monitoring_sheet()
        {
            if(isset($_POST['path']))
            {
                
                $path = json_decode($_POST['path'], true);

                $file_title = $path[ sizeof($path) - 1 ];
                $filename   = $path[ sizeof($path) - 1 ] . ".pdf";

                $data = (func_num_args() > 0) ? func_get_arg(0) : false;
                $data = $this->monitoring_sheet_data( $data );

                $pdf = new FPDF('P','mm',array(100,150));       
                $pdf->AddPage("P");    
                $pdf->SetMargins(1.5, 1, 1.5);
                $pdf->SetTitle($file_title);
                $pdf->SetLineWidth(0.1);
                $pageW = $pdf->GetPageWidth();
                $pdf->Ln();

                $slash = Settings::getSeparator();
                $logo_dir = implode( $slash, [ getcwd(), 'Resource', 'img', 'logo.jpg' ] );
                $pdf->Image( $logo_dir ,10,5,-300);
                $pdf->Ln();

                $pdf->SetFont("Arial","", 5);
                $pdf->SetXY( 45, 17 );

                $pdf->Cell(13, 2, "Case Number:", 0, 0, "L", 0);

                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(20, 2, $data['case_no'] , "B", 1, "C", 0 );
                $pdf->SetFont("Arial","", 5);            

                $pdf->Ln();

                $pdf->SetXY( 1.5, 23 );            
                $pdf->Cell(15, 2, "Name of Patient:", 0, 0, "L", 0);
                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(50, 2, $data['patient'] , "B", 0, "C", 0 );
                $pdf->SetFont("Arial","", 5);            

                $pdf->Cell(6, 2, "Age:", 0, 0, "L", 0);

                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(20, 2, $data['age'] , "B", 1, "C", 0 );

                $pdf->SetXY( 1.5, 28 );                        
                $pdf->SetFont("Arial","", 5);                       
                $pdf->Cell(15, 2, "Address:", 0, 0, "L", 0);
                $pdf->SetFont("Courier","", 5);                        
                $pdf->Cell(76, 2, $data['address'] , "B", 0, "C", 0 );

                $pdf->Ln();
                $pdf->SetLineWidth(0.5);
                $pdf->Line( 1.5 , 35 , 97, 35);

                $pdf->Ln();            
                $pdf->SetXY( 1.5, 38 );  
                $pdf->SetFont("Arial","B", 7);                       
                $pdf->Cell(97, 2, strtoupper($file_title), 0, 1, "C", 0);
                $pdf->Ln();         

                $pdf->SetFont("Arial","B", 5);                       
                $pdf->SetLineWidth(0.1);

                $pdf->Cell(15 , 3, "Date/Shift", "LT", 0, "C", 0);
                $pdf->Cell(8 , 3, "Time", "LT", 0, "C", 0);
                $pdf->Cell(33 , 3, "Vital Signs", "LTB", 0, "C", 0);
                $pdf->Cell(40 , 3, "Uterine Contractions", "LTBR", 1, "C", 0);

                $pdf->Cell(15 , 3, "", "LB", 0, "C", 0);
                $pdf->Cell(8 , 3, "", "LB", 0, "C", 0);

                $pdf->Cell(6.6 , 3, "BP", "LB", 0, "C", 0);
                $pdf->Cell(6.6 , 3, "PR", "LB", 0, "C", 0);
                $pdf->Cell(6.6 , 3, "RR", "LB", 0, "C", 0);
                $pdf->Cell(6.6 , 3, "T", "LB", 0, "C", 0);
                $pdf->Cell(6.6 , 3, "FHT", "LB", 0, "C", 0);

                $pdf->Cell(10 , 3, "Duration", "LB", 0, "C", 0);
                $pdf->Cell(10 , 3, "Interval", "LB", 0, "C", 0);
                $pdf->Cell(10 , 3, "Frequency", "LB", 0, "C", 0);
                $pdf->Cell(10 , 3, "Intensity", "LBR", 0, "C", 0);
                $pdf->Ln();

                $pdf->SetFont( "Courier", "", 4 );

                $break = [ 2, 7, 10, 15, 18, 23 ];
                $counter = 0;
                foreach($data['data'] as $record )
                {
                    $border = in_array( $counter, $break) ? "LB" : "L"; 

                    $pdf->Cell(15 , 3, $record['date'], $border, 0, "C", 0);
                    $pdf->Cell(8 , 3, $record['time'], "LB", 0, "C", 0);

                    $pdf->Cell(6.6 , 3, $record['bp'], "LB", 0, "C", 0);
                    $pdf->Cell(6.6 , 3, $record['pr'], "LB", 0, "C", 0);
                    $pdf->Cell(6.6 , 3, $record['rr'], "LB", 0, "C", 0);
                    $pdf->Cell(6.6 , 3, $record['t'], "LB", 0, "C", 0);
                    $pdf->Cell(6.6 , 3, $record['fht'], "LB", 0, "C", 0);

                    $pdf->Cell(10 , 3, $record['duration'], "LB", 0, "C", 0);
                    $pdf->Cell(10 , 3, $record['interval'], "LB", 0, "C", 0);
                    $pdf->Cell(10 , 3, $record['frequency'], "LB", 0, "C", 0);
                    $pdf->Cell(10 , 3, $record['intensity'], "LBR", 0, "C", 0);

                    $pdf->Ln();
                    $counter++;
                }

                $path = implode(Settings::getSeparator(), $path ) . ".pdf";
                $pdf->Output("F", $path );
                
                Encoder::set(['status' => 200, 'message' => 'File successfully generated.']);
            }else
            {
                Encoder::mpd();
            }
            
            
        }
        function monitoring_sheet_data($data)
        {
            $record = [];

            if(isset($data['data']))
            {
                $count = sizeof($data['data']);
                $record = $data['data'];

                for($i = $count; $i < 24; $i++)
                {
                    array_push( $record, [ 'date' => '', 'time' => '', 'bp' => '', 'pr' => '', 'rr' => '', 't'=> '', 'fht' => '', 'duration' => '', 'interval' => '', 'frequency'=> '', 'intensity' => '' ] );
                }

                $data['data'] = $record;
            }else
            {
                for($i = 0; $i < 24; $i++)
                {
                    array_push( $record, [ 'date' => '', 'time' => '', 'bp' => '', 'pr' => '', 'rr' => '', 't'=> '', 'fht' => '', 'duration' => '', 'interval' => '', 'frequency'=> '', 'intensity' => '' ] );
                }                
            }


            return (!$data) ? [
                'case_no' => '12-456454',
                'patient' => 'Gerald Jorge Pamga',
                'age'     => '24',
                'address' => 'Dayangdang Naga',
                'data' => $record 
                
            ] : $data;
        }

        function midwife_notes()
        {
            if(isset($_POST['path']))
            {
                
                $path = json_decode($_POST['path'], true);

                $file_title = $path[ sizeof($path) - 1 ];
                $filename   = $path[ sizeof($path) - 1 ] . ".pdf";

                $data = (func_num_args() > 0) ? func_get_arg(0) : false;
                $data = $this->midwife_notes_data( $data );

                $pdf = new FPDF('P','mm',array(100,150));       
                $pdf->AddPage("P");    
                $pdf->SetMargins(1.5, 1, 1.5);
                $pdf->SetTitle($file_title);
                $pdf->SetLineWidth(0.1);
                $pageW = $pdf->GetPageWidth();
                $pdf->Ln();

                $slash = Settings::getSeparator();
                $logo_dir = implode( $slash, [ getcwd(), 'Resource', 'img', 'logo.jpg' ] );
                $pdf->Image( $logo_dir ,10,5,-300);
                $pdf->Ln();

                $pdf->SetFont("Arial","", 5);
                $pdf->SetXY( 45, 17 );

                $pdf->Cell(13, 2, "Case Number:", 0, 0, "L", 0);

                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(20, 2, $data['case_no'] , "B", 1, "C", 0 );
                $pdf->SetFont("Arial","", 5);            

                $pdf->Ln();

                $pdf->SetXY( 1.5, 23 );            
                $pdf->Cell(15, 2, "Name of Patient:", 0, 0, "L", 0);
                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(50, 2, $data['patient'] , "B", 0, "C", 0 );
                $pdf->SetFont("Arial","", 5);            

                $pdf->Cell(6, 2, "Age:", 0, 0, "L", 0);

                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(20, 2, $data['age'] , "B", 1, "C", 0 );

                $pdf->SetXY( 1.5, 28 );                        
                $pdf->SetFont("Arial","", 5);                       
                $pdf->Cell(15, 2, "Address:", 0, 0, "L", 0);
                $pdf->SetFont("Courier","", 5);                        
                $pdf->Cell(76, 2, $data['address'] , "B", 0, "C", 0 );

                $pdf->Ln();
                $pdf->SetLineWidth(0.5);
                $pdf->Line( 1.5 , 35 , 97, 35);

                $pdf->Ln();            
                $pdf->SetXY( 1.5, 38 );  
                $pdf->SetFont("Arial","B", 7);                       
                $pdf->Cell(97, 2, strtoupper($file_title), 0, 1, "C", 0);
                $pdf->Ln();

                $pdf->SetXY( 4.0, 45 );              
                $pdf->SetLineWidth(0.1);

                $pdf->SetFont("Arial","B", 5);                       
                $pdf->Cell(20 , 5, "DATE/SHIFT/TIME", "LTRB", 0, "C", 0);
                $pdf->Cell(70 , 5, "NOTES", "TRB", 1, "C", 0);

                $pdf->Rect( 4.0, 50, 20, 90, "D");
                $pdf->Rect( 24, 50, 70, 90, "D");

                $pdf->SetFont("Courier","", 5);                       

                $x = 4.0; $y = 52;
                foreach($data['notes'] as $order)
                {
                    $x = 4.0;
                    $pdf->SetY($y);
                    $pdf->SetX($x); $x+=20;
                    $pdf->MultiCell( 20 , 2, $order['date'] ,0,'C', 0);

                    $noteText = $order['note'];

                    $pdf->SetY($y);
                    $pdf->SetX($x);
                    $pdf->MultiCell( 70 , 2, $noteText ,0,'L', 0);                

                    $toAdd = (sizeof(explode("\n", $noteText )) * 2) + ( $pdf->GetStringWidth( $noteText ) / 30 );
                    $y += ( $toAdd );       
                }

                $path = implode(Settings::getSeparator(), $path ) . ".pdf";
                $pdf->Output("F", $path );
                
                Encoder::set(['status' => 200, 'message' => 'File successfully generated.']);
            }else
            {
                Encoder::mpd();
            }
            
        }
        function midwife_notes_data($data)
        {
            return (!$data) ? [
                'case_no' => '',
                'patient' => '',
                'age'     => '',
                'address' => '',
                'notes'  => [
                    ['date' => '', 'note' => ""],
                   ]
            ] : $data;
        }

        function postpartum()
        {
            if(isset($_POST['path']))
            {
                
                $path = json_decode($_POST['path'], true);

                $file_title = $path[ sizeof($path) - 1 ];
                $filename   = $path[ sizeof($path) - 1 ] . ".pdf";

                $data = (func_num_args() > 0) ? func_get_arg(0) : false;
                $data = $this->postpartum_data( $data );

                $pdf = new FPDF('P','mm',array(100,150));       
                $pdf->AddPage("P");    
                $pdf->SetMargins(1.5, 1, 1.5);
                $pdf->SetTitle($file_title);
                $pdf->SetLineWidth(0.1);
                $pageW = $pdf->GetPageWidth();
                $pdf->Ln();

                $slash = Settings::getSeparator();
                $logo_dir = implode( $slash, [ getcwd(), 'Resource', 'img', 'logo.jpg' ] );
                $pdf->Image( $logo_dir ,10,5,-300);
                $pdf->Ln();

                $pdf->SetFont("Arial","", 5);
                $pdf->SetXY( 45, 17 );

                $pdf->Cell(13, 2, "Case Number:", 0, 0, "L", 0);

                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(20, 2, $data['case_no'] , "B", 1, "C", 0 );
                $pdf->SetFont("Arial","", 5);            

                $pdf->Ln();

                $pdf->SetXY( 1.5, 23 );            
                $pdf->Cell(15, 2, "Name of Patient:", 0, 0, "L", 0);
                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(50, 2, $data['patient'] , "B", 0, "C", 0 );
                $pdf->SetFont("Arial","", 5);            

                $pdf->Cell(6, 2, "Age:", 0, 0, "L", 0);

                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(20, 2, $data['age'] , "B", 1, "C", 0 );

                $pdf->SetXY( 1.5, 28 );                        
                $pdf->SetFont("Arial","", 5);                       
                $pdf->Cell(15, 2, "Address:", 0, 0, "L", 0);
                $pdf->SetFont("Courier","", 5);                        
                $pdf->Cell(76, 2, $data['address'] , "B", 0, "C", 0 );

                $pdf->Ln();
                $pdf->SetLineWidth(0.5);
                $pdf->Line( 1.5 , 35 , 97, 35);

                $pdf->Ln();            
                $pdf->SetXY( 1.5, 38 );  
                $pdf->SetFont("Arial","B", 7);                       
                $pdf->Cell(97, 2, strtoupper($file_title), 0, 1, "C", 0);
                $pdf->Ln();

                $pdf->SetXY( 4.0, 45 );              
                $pdf->SetLineWidth(0.1);

                $pdf->SetFont("Arial","B", 5);                       
                $pdf->Cell(20 , 5, "DATE/SHIFT/TIME", "LTRB", 0, "C", 0);
                $pdf->Cell(70 , 5, "NOTES", "TRB", 1, "C", 0);

                $pdf->Rect( 4.0, 50, 20, 90, "D");
                $pdf->Rect( 24, 50, 70, 90, "D");

                $pdf->SetFont("Courier","", 5);                       

                $x = 4.0; $y = 52;
                foreach($data['notes'] as $order)
                {
                    $x = 4.0;
                    $pdf->SetY($y);
                    $pdf->SetX($x); $x+=20;
                    $pdf->MultiCell( 20 , 2, $order['date'] ,0,'C', 0);

                    $noteText = $order['note'];

                    $pdf->SetY($y);
                    $pdf->SetX($x);
                    $pdf->MultiCell( 70 , 2, $noteText ,0,'L', 0);                

                    $toAdd = (sizeof(explode("\n", $noteText )) * 2) + ( $pdf->GetStringWidth( $noteText ) / 30 );
                    $y += ( $toAdd );       
                }
                
                $path = implode(Settings::getSeparator(), $path ) . ".pdf";
                $pdf->Output("F", $path );
                
                Encoder::set(['status' => 200, 'message' => 'File successfully generated.']);
            }else
            {
                Encoder::mpd();
            }
        }
        function postpartum_data($data)
        {
            return (!$data) ? [
                'case_no' => '',
                'patient' => '',
                'age'     => '',
                'address' => '',
                'notes'  => [
                    ['date' => '', 'note' => ""],
                   ]
            ] : $data;
        }

        function monitoring_sheet_baby()
        {
            if(isset($_POST['path']))
            {
                
                $path = json_decode($_POST['path'], true);

                $file_title = $path[ sizeof($path) - 1 ];
                $filename   = $path[ sizeof($path) - 1 ] . ".pdf";

            
                $data = (func_num_args() > 0) ? func_get_arg(0) : false;
                $data = $this->monitoring_sheet_baby_data( $data );

                $pdf = new FPDF('P','mm',array(100,150));       
                $pdf->AddPage("P");    
                $pdf->SetMargins(1.5, 1, 1.5);
                $pdf->SetTitle($file_title);
                $pdf->SetLineWidth(0.1);
                $pageW = $pdf->GetPageWidth();
                $pdf->Ln();

                $slash = Settings::getSeparator();
                $logo_dir = implode( $slash, [ getcwd(), 'Resource', 'img', 'logo.jpg' ] );
                $pdf->Image( $logo_dir ,10,5,-300);
                $pdf->Ln();

                $pdf->SetFont("Arial","", 5);
                $pdf->SetXY( 45, 17 );

                $pdf->Cell(13, 2, "Case Number:", 0, 0, "L", 0);

                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(20, 2, $data['case_no'] , "B", 1, "C", 0 );
                $pdf->SetFont("Arial","", 5);            

                $pdf->Ln();

                $pdf->SetXY( 1.5, 23 );            
                $pdf->Cell(15, 2, "Name of Patient:", 0, 0, "L", 0);
                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(50, 2, $data['patient'] , "B", 0, "C", 0 );
                $pdf->SetFont("Arial","", 5);            

                $pdf->Cell(6, 2, "Age:", 0, 0, "L", 0);

                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(20, 2, $data['age'] , "B", 1, "C", 0 );

                $pdf->SetXY( 1.5, 28 );                        
                $pdf->SetFont("Arial","", 5);                       
                $pdf->Cell(15, 2, "Address:", 0, 0, "L", 0);
                $pdf->SetFont("Courier","", 5);                        
                $pdf->Cell(76, 2, $data['address'] , "B", 0, "C", 0 );

                $pdf->Ln();
                $pdf->SetLineWidth(0.5);
                $pdf->Line( 1.5 , 35 , 97, 35);

                $pdf->Ln();            
                $pdf->SetXY( 1.5, 38 );  
                $pdf->SetFont("Arial","B", 7);                       
                $pdf->Cell(97, 2, strtoupper($file_title), 0, 1, "C", 0);
                $pdf->Ln();         

                $pdf->SetFont("Arial","B", 4);                       
                $pdf->SetLineWidth(0.1);

                $pdf->Cell(15 , 3, "Date/Shift", "LT", 0, "C", 0);
                $pdf->Cell(8 , 3, "Time", "LT", 0, "C", 0);
                $pdf->Cell(40 , 3, "Vital Signs", "LTB", 0, "C", 0);
                $pdf->Cell(33 , 3, "Output", "LTBR", 1, "C", 0);

                $pdf->Cell(15 , 3, "", "LB", 0, "C", 0);
                $pdf->Cell(8 , 3, "", "LB", 0, "C", 0);

                $pdf->Cell(13.33 , 3, "Cardiac Rate", "LB", 0, "C", 0);
                $pdf->Cell(13.33 , 3, "Respiratory Rate", "LB", 0, "C", 0);
                $pdf->Cell(13.33 , 3, "Temparature", "LB", 0, "C", 0);

                $pdf->Cell(11 , 3, "Feeding", "LB", 0, "C", 0);
                $pdf->Cell(11 , 3, "Urine", "LB", 0, "C", 0);
                $pdf->Cell(11 , 3, "Stool", "LBR", 0, "C", 0);
                $pdf->Ln();

                $pdf->SetFont( "Courier", "", 4 );

                $break = [ 2, 7, 10, 15, 18, 23 ];
                $counter = 0;
                foreach($data['data'] as $record )
                {
                    $border = in_array( $counter, $break) ? "LB" : "L"; 

                    $pdf->Cell(15 , 3, $record['date'] , $border, 0, "C", 0);
                    $pdf->Cell(8 , 3, $record['time'] , "LB", 0, "C", 0);

                    $pdf->Cell(13.33 , 3, $record['cardiac'], "LB", 0, "C", 0);
                    $pdf->Cell(13.33 , 3, $record['respiratory'], "LB", 0, "C", 0);
                    $pdf->Cell(13.33 , 3, $record['temperature'], "LB", 0, "C", 0);

                    $pdf->Cell(11 , 3, $record['feeding'], "LB", 0, "C", 0);
                    $pdf->Cell(11 , 3, $record['urine'], "LB", 0, "C", 0);
                    $pdf->Cell(11 , 3, $record['stool'], "LBR", 0, "C", 0);

                    $pdf->Ln();
                    $counter++;
                }

                $path = implode(Settings::getSeparator(), $path ) . ".pdf";
                $pdf->Output("F", $path );
                
                Encoder::set(['status' => 200, 'message' => 'File successfully generated.']);
            }else
            {
                Encoder::mpd();
            }
        }
        function monitoring_sheet_baby_data($data)
        {
            $record = [];

            if(isset($data['data']))
            {
                $count = sizeof($data['data']);
                $record = $data['data'];

                for($i = $count; $i < 24; $i++)
                {
                    array_push( $record, [ 'date' => '', 'time' => '', 'cardiac' => '', 'respiratory' => '', 'temperature' => '', 'feeding'=> '', 'urine' => '', 'stool' => '' ] );
                }

                $data['data'] = $record;
            }else
            {
                for($i = 0; $i < 24; $i++)
                {
                    array_push( $record, [ 'date' => '', 'time' => '', 'cardiac' => '', 'respiratory' => '', 'temperature' => '', 'feeding'=> '', 'urine' => '', 'stool' => '' ] );
                }                
            }


            return (!$data) ? [
                'case_no' => '',
                'patient' => '',
                'age'     => '',
                'address' => '',
                'data' => $record 
                
            ] : $data;
        }

        function nursery_notes()
        {
            if(isset($_POST['path']))
            {
                
                $path = json_decode($_POST['path'], true);

                $file_title = $path[ sizeof($path) - 1 ];
                $filename   = $path[ sizeof($path) - 1 ] . ".pdf";

                $data = (func_num_args() > 0) ? func_get_arg(0) : false;
                $data = $this->nursery_notes_data( $data );

                $pdf = new FPDF('P','mm',array(100,150));       
                $pdf->AddPage("P");    
                $pdf->SetMargins(1.5, 1, 1.5);
                $pdf->SetTitle($file_title);
                $pdf->SetLineWidth(0.1);
                $pageW = $pdf->GetPageWidth();
                $pdf->Ln();

                $slash = Settings::getSeparator();
                $logo_dir = implode( $slash, [ getcwd(), 'Resource', 'img', 'logo.jpg' ] );
                $pdf->Image( $logo_dir ,10,5,-300);
                $pdf->Ln();

                $pdf->SetFont("Arial","", 5);
                $pdf->SetXY( 45, 17 );

                $pdf->Cell(13, 2, "Case Number:", 0, 0, "L", 0);

                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(20, 2, $data['case_no'] , "B", 1, "C", 0 );
                $pdf->SetFont("Arial","", 5);            

                $pdf->Ln();

                $pdf->SetXY( 1.5, 23 );            
                $pdf->Cell(15, 2, "Name of Patient:", 0, 0, "L", 0);
                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(50, 2, $data['patient'] , "B", 0, "C", 0 );
                $pdf->SetFont("Arial","", 5);            

                $pdf->Cell(6, 2, "Age:", 0, 0, "L", 0);

                $pdf->SetFont("Courier","", 5);            
                $pdf->Cell(20, 2, $data['age'] , "B", 1, "C", 0 );

                $pdf->SetXY( 1.5, 28 );                        
                $pdf->SetFont("Arial","", 5);                       
                $pdf->Cell(15, 2, "Address:", 0, 0, "L", 0);
                $pdf->SetFont("Courier","", 5);                        
                $pdf->Cell(76, 2, $data['address'] , "B", 0, "C", 0 );

                $pdf->Ln();
                $pdf->SetLineWidth(0.5);
                $pdf->Line( 1.5 , 35 , 97, 35);

                $pdf->Ln();            
                $pdf->SetXY( 1.5, 38 );  
                $pdf->SetFont("Arial","B", 7);                       
                $pdf->Cell(97, 2, strtoupper($file_title), 0, 1, "C", 0);
                $pdf->Ln();

                $pdf->SetXY( 4.0, 45 );              
                $pdf->SetLineWidth(0.1);

                $pdf->SetFont("Arial","B", 5);                       
                $pdf->Cell(20 , 5, "DATE/SHIFT/TIME", "LTRB", 0, "C", 0);
                $pdf->Cell(70 , 5, "NOTES", "TRB", 1, "C", 0);

                $pdf->Rect( 4.0, 50, 20, 90, "D");
                $pdf->Rect( 24, 50, 70, 90, "D");

                $pdf->SetFont("Courier","", 5);                       

                $x = 4.0; $y = 52;
                foreach($data['notes'] as $order)
                {
                    $x = 4.0;
                    $pdf->SetY($y);
                    $pdf->SetX($x); $x+=20;
                    $pdf->MultiCell( 20 , 2, $order['date'] ,0,'C', 0);

                    $noteText = $order['note'];

                    $pdf->SetY($y);
                    $pdf->SetX($x);
                    $pdf->MultiCell( 70 , 2, $noteText ,0,'L', 0);                

                    $toAdd = (sizeof(explode("\n", $noteText )) * 2) + ( $pdf->GetStringWidth( $noteText ) / 30 );
                    $y += ( $toAdd );       
                }
                
                $path = implode(Settings::getSeparator(), $path ) . ".pdf";
                $pdf->Output("F", $path );
                
                Encoder::set(['status' => 200, 'message' => 'File successfully generated.']);
            }else
            {
                Encoder::mpd();
            }
            
        }
        function nursery_notes_data($data)
        {
            return (!$data) ? [
                'case_no' => '',
                'patient' => '',
                'age'     => '',
                'address' => '',
                'notes'  => [
                    ['date' => '', 'note' => ""],
                   ]
            ] : $data;
        }
    }
    
?>
