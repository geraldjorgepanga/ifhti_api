<?php

	class PatientService extends _init
    {
		function __construct()
		{
			//Initialize Object;
			$this->init();
		}
		function init()
		{
			$this->tableName = "tbl_patient_service";
			/** This variable is used in generating Unique ID for these Class **/
			$this->prefix = "PS";
			/** These variables are important for fetching complete data; **/
			$this->pk = "ps_id";
			$this->fk = [
				[ 'pk' => 'p_id', 'model' => 'Patient', 'table' => 'patient' ],
				[ 'pk' => 's_id', 'model' => 'Service', 'table' => 'service' ]
			];
			/** Unique ID Added Random String Length **/
			$this->gidlen = 3;	
					   /**  
		   		Initialize Required Post Data  
			    $this->int->struct is the DEFAULT value every time ADD is called
			**/
  			$this->int = ( object )
  			[
  				'struct' => [
                            $this->pk => $this->generateId() ,
                            'p_id' => '',
                            's_id' => '',
							'ps_date' => '',
                            'ps_hid' => false,
							'with_philhealth' => '0',
                            'ps_created' => Time::getTime(8)
  						 ],
                  'add' => ['p_id', 's_id', 'ps_date', 'with_philhealth' ],
			   'update' => [ $this->pk, 'p_id', 's_id', 'ps_hid', 'ps_date', 'with_philhealth' ],
			   'delete' => [ $this->pk ] ,
			   'get'	=> [ 'method', 'params' ]
  			];
			$this->after_action = 'createFile';
		}
		
		function createFile()
		{
			if( isset($this->postData['s_id']) && 
			    isset($this->postData['ps_date']) && 
			    isset($this->postData['b_id']) &&
			   	isset($this->postData['p_id'])
			  )
			{
				$file = Model::object( 'File' );
				$file->createPatientServiceRecord([ 
					'Data', 'Patients', $this->postData['b_id'], 
					$this->postData['p_id'], $this->postData['s_id'],
					$this->postData['ps_date']
				]);
			}
		}
	}
?>
