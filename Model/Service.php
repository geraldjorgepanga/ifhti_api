<?php

	class Service extends _init
    {
		function __construct()
		{
			//Initialize Object;
			$this->init();
		}
		function init()
		{
			$this->tableName = "tbl_service";
			/** This variable is used in generating Unique ID for these Class **/
			$this->prefix = "SRV";
			/** These variables are important for fetching complete data; **/
			$this->pk = "s_id";
			$this->fk = [
			];
			/** Unique ID Added Random String Length **/
			$this->gidlen = 3;	
					   /**  
		   		Initialize Required Post Data  
			    $this->int->struct is the DEFAULT value every time ADD is called
			**/
  			$this->int = ( object )
  			[
  				'struct' => [
                            $this->pk => $this->generateId(),
                            's_name' => '',
                            's_created' => Time::getTime(8),
                            's_hid' => 0
  						 ],

                  'add' => ['s_name'],
			   'update' => [ $this->pk, 's_name' ],
			   'delete' => [ $this->pk ] ,
			   'get'	=> [ 'method', 'params' ],
			   'all'	=> [ 'chunk' ]
  			];
		}
		function all()
		{
			if( Model::requiredPost( $this->int->all ) )
			{
				$postData = Model::getPostData();
				
				if($this->isAuthorized( $postData['chunk'] ))
				{
					$this->printAllData();					
				}else
				{
					Encoder::set(['status' => 400, 'message'=> 'You are not authorized!']);
				}

			}else
			{
				Encoder::invalid();
			}
		}
	}
?>
