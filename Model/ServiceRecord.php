<?php

	class ServiceRecord extends _init
    {
		function __construct()
		{
			//Initialize Object;
			$this->init();
		}
		function init()
		{
			$this->tableName = "tbl_service_record";
			/** This variable is used in generating Unique ID for these Class **/
			$this->prefix = "SR";
			/** These variables are important for fetching complete data; **/
			$this->pk = "sr_id";
			$this->fk = [
				[ 'pk' => 's_id', 'model' => 'Service', 'table' => 'service' ]
			];
			/** Unique ID Added Random String Length **/
			$this->gidlen = 3;	
					   /**  
		   		Initialize Required Post Data  
			    $this->int->struct is the DEFAULT value every time ADD is called
			**/
  			$this->int = ( object )
  			[
  				'struct' => [
                            $this->pk => $this->generateId() ,
                            'sr_date' => '',
                            'sr_name' => '',
                            'sr_status' => '',
                            'sr_created' => Time::getTime(8),
							'sr_hid' => false,
							's_id' => ''
  						 ],
                  'add' => [ 'sr_date', 'sr_name', 'sr_status', 's_id' ],
			   'update' => [ $this->pk, 'sr_date', 'sr_name', 'sr_status', 's_id', 'sr_hid' ],
			   'delete' => [ $this->pk ] ,
			   'get'	=> [ 'method', 'params' ]
  			];
		}
	}
?>
