<?php

	class News extends _init
    {
		function __construct()
		{
			//Initialize Object;
			$this->init();
		}
		function init()
		{
			$this->tableName = "tbl_news";
			/** This variable is used in generating Unique ID for these Class **/
			$this->prefix = "NW";
			/** These variables are important for fetching complete data; **/
			$this->pk = "news_id";
			$this->fk = [
				[ 'pk' => 'b_id', 'model' => 'Branch', 'table' => 'branch' ]
			];
			/** Unique ID Added Random String Length **/
			$this->gidlen = 3;	
					   /**  
		   		Initialize Required Post Data  
			    $this->int->struct is the DEFAULT value every time ADD is called
			**/
  			$this->int = ( object )
  			[
  				'struct' => [
                            $this->pk => $this->generateId() ,
                            'b_id' => '',
                            'news_title' => '',
                            'news_content' => '',
                            'news_file' => '',
                            'news_posted' => Time::getTime(8)
  						 ],

			   'add' => ['b_id', 'news_title', 'news_content', 'news_posted', 'news_file' ],
			   'update' => [ $this->pk, 'b_id', 'news_title', 'news_content', 'news_file' ],
			   'delete' => [ $this->pk ] ,
			   'get'	=> [ 'method', 'params' ],
			   'all'	=> ['chunk']
  			];
		}
		function all()
		{
			if( Model::requiredPost( $this->int->all ) )
			{
				$postData = Model::getPostData();
				
				if($this->isAuthorized( $postData['chunk'] ))
				{
					$this->printAllData();					
				}else
				{
					Encoder::set(['status' => 400, 'message'=> 'You are not authorized!']);
				}

			}else
			{
				Encoder::invalid();
			}
		}
	}
?>
