<?php

    class Payment extends _init 
    {

        function __construct()
		{
			//Initialize Object;
			$this->init();
        }
        
        function init() 
        {
            $this->tableName = "tbl_payment";

            $this->prefix = "PAY";

            $this->pk = "pay_no";

            $this->fk = [            
                ['pk' => 'ps_id', 'model' => 'PatientService', 'table' => 'patient_service_record']
            ];

            $this->gidlen = 3;

            $this->int = ( object )
  			[
  				'struct' => [
                            $this->pk => $this->generateId() ,
                            'p_id' => '',
                            'ps_id' => '',
                            'pay_date' => '',
                            'pay_amount' => '',
                            'pay_remarks' => '',
                            'pay_created' => Time::getTime(8),
                            'pay_hid' => false
  						 ],
                  'add' => ['p_id', 'ps_id', 'pay_date', 'pay_amount', 'pay_remarks'],
			   'update' => [ $this->pk, 'p_id', 'ps_id', 'pay_date', 'pay_amount', 'pay_remarks', 'pay_hid' ],
			   'delete' => [ $this->pk ] ,
               'get'	=> [ 'method', 'params' ],
               'all'    => [ 'chunk' ]
  			];
        }

        function all()
		{
			if( Model::requiredPost( $this->int->all ) )
			{
				$postData = Model::getPostData();
				
				if($this->isAuthorized( $postData['chunk'] ))
				{
					$this->printAllData();					
				}else
				{
					Encoder::set(['status' => 400, 'message'=> 'You are not authorized!']);
				}

			}else
			{
				Encoder::invalid();
			}
		}

    }

?>