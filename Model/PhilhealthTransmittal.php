<?php

	class PhilhealthTransmittal extends _init
    {
		function __construct()
		{
			//Initialize Object;
			$this->init();
		}
		function init()
		{
			$this->tableName = "tbl_philhealth_transmittal";
			/** This variable is used in generating Unique ID for these Class **/
			$this->prefix = "PHT";
			/** These variables are important for fetching complete data; **/
			$this->pk = "pt_id";
			$this->fk = [
			];
			/** Unique ID Added Random String Length **/
			$this->gidlen = 3;	
			/**  
		   		Initialize Required Post Data  
			    $this->int->struct is the DEFAULT structure and value;
			**/
  			$this->int = ( object )
  			[
  				'struct' => [
                            $this->pk => $this->generateId() ,
                            'pt_name' => '',
                            'pt_created' => Time::getTime(8)
  						 ],
                  'add' => ['pt_name'],
			   'update' => [ $this->pk, 'pt_name'],
			   'delete' => [ $this->pk ],
			   	  'get' => [ 'method', 'params' ],
		   	 	  'all' => ['chunk']
  			];
		}
		function all()
		{
			if( Model::requiredPost( $this->int->all ) )
			{
				$postData = Model::getPostData();
				if($this->isAuthorized( $postData['chunk'] ))
				{
					$this->printAllData();					
				}else
				{
					Encoder::set(['status' => 400, 'message'=> 'You are not authorized!']);
				}
			}else
			{
				Encoder::invalid();
			}
		}	
		function write_ini_file($assoc_arr, $path, $has_sections=FALSE) 
		{ 
			$content = ""; 
			if ($has_sections) { 
				foreach ($assoc_arr as $key=>$elem) { 
					$content .= "[".$key."]\n"; 
					foreach ($elem as $key2=>$elem2) { 
						if(is_array($elem2)) 
						{ 
							for($i=0;$i<count($elem2);$i++) 
							{ 
								$content .= $key2."[] = \"".$elem2[$i]."\"\n"; 
							} 
						} 
						else if($elem2=="") $content .= $key2." = \n"; 
						else $content .= $key2." = \"".$elem2."\"\n"; 
					} 
				} 
			} 
			else { 
				foreach ($assoc_arr as $key=>$elem) { 
					if(is_array($elem)) 
					{ 
						for($i=0;$i<count($elem);$i++) 
						{ 
							$content .= $key."[] = \"".$elem[$i]."\"\n"; 
						} 
					} 
					else if($elem=="") $content .= $key." = \n"; 
					else $content .= $key." = \"".$elem."\"\n"; 
				} 
			} 

			if (!$handle = fopen($path, 'w')) { 
				return false; 
			}

			$success = fwrite($handle, $content);
			fclose($handle); 

			return $success; 
		}
		function readDetails($object)
		{
			if(isset($object['dir']))
			{
				$root = [getcwd()];
				$slash = Settings::getSeparator();
				$root = array_merge( $root, $object['dir'] );
				$directory = implode($slash, $root);
				
				if(!file_exists( $directory ))
				{
					$this->write_ini_file( $this->defaultDetails() , $directory, true );
				}
				
				Encoder::set( ['status' => 200, 'message' => 'Success', 'data' => parse_ini_file($directory)] );
				
			}else
			{
				Encoder::mpd();
			}
		}
		function updateDetails($object)
		{
			if(isset($object['dir']) && isset($object['data']))
			{
				$root = [getcwd()];
				$slash = Settings::getSeparator();
				$root = array_merge( $root, $object['dir'] );
				
				if($this->write_ini_file( $object['data'], implode($slash, $root), true ))
				{
					Encoder::set(['status' => 200, 'message' => 'Success']);
				}else
				{
					Encoder::set(['status' => 400, 'message' => 'Error']);
				}
			}else
			{
				Encoder::mpd();
			}
		}
		function defaultDetails()
		{
			$details = [
				'philhealth_transmittal' => [
					'payment_status' => '0',
					'transmittal_date' => '',
					'payment_date' => '',
					'check_no' => '',
					'trans_no' => '',
					'remarks' => ''
				]
			];
			return $details;
		}
		
	}
?>
