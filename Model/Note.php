<?php

	class Note extends _init
    {
		function __construct()
		{
			//Initialize Object;
			$this->init();
		}
		function init()
		{
			$this->tableName = "tbl_note";
			/** This variable is used in generating Unique ID for these Class **/
			$this->prefix = "NT";
			/** These variables are important for fetching complete data; **/
			$this->pk = "n_id";
			$this->fk = [
				[ 'pk' => 'b_id', 'model' => 'Branch', 'table' => 'branch' ]
			];
			/** Unique ID Added Random String Length **/
			$this->gidlen = 3;	
					   /**  
		   		Initialize Required Post Data  
			    $this->int->struct is the DEFAULT value every time ADD is called
			**/
  			$this->int = ( object )
  			[
  				'struct' => [
                            $this->pk => $this->generateId() ,
                            'b_id' => '',
                            'n_note' => '',
                            'n_author' => '',
                            'n_date' => '',
                            'n_timein' => '',
							'n_timeout' => '',
							'n_created' => Time::getTime(8),
							'n_hid' => false
  						 ],
                  'add' => ['b_id', 'n_note', 'n_author', 'n_date', 'n_timein', 'n_timeout', 'n_type' ],
			   'update' => [ $this->pk, 'b_id', 'n_note', 'n_author', 'n_date', 'n_timein', 'n_timeout', 'n_hid' ],
			   'delete' => [ $this->pk ] ,
			   'get'	=> [ 'method', 'params', 'chunk' ],
			   'years'  => [ 'type' ]
  			];
		}
		function years()
		{
			if(Model::requiredPost( $this->int->years ))
			{
				$postData = Model::getPostData();
				$query = "SELECT SUBSTR( n_date, 1, 4 ) as _year from tbl_note WHERE n_type=:n_type GROUP BY _year";
				$data = DBLib::execute( $query, [ 'n_type' => $postData['type'] ] );

				Encoder::set( DBLib::toArray($data) );
				
			}else
			{
				Encoder::mpd();
			}
		}
		function getDataByDate( $date )
		{
			$data = DBlib::toArray( 
				DBLib::select( 
					$this->tableName, '*', ['n_date' => $date['date'], 'n_type' => $date['type'], 'b_id'=>$date['b_id'], 'n_hid' => 0] 
				) 
			);
			
			Encoder::set( $data );
		}
		/**
			Object
			 @month | string
			 @year 	| string
		**/
		function getDataByDates( $object )
		{
			$data = DBLib::betweenDates( $this->tableName, 'n_date', $object['from'], $object['to'] );

			$response = [];
			if( isset($data['data']) )
			{
				$response = ['status' => $data['status'], 'message' => $data['message']];
				$list = [];
				foreach($data['data'] as $record)
				{
					if( $object['type'] == $record['n_type'] )
					{
						if( $object['b_id'] == $record['b_id'] )
						{
							if( $record['n_hid'] == "0" )
							{
								array_push( $list, $record );								
							}
						}
					}
				}				
				$response['data'] = $list;
			}else
			{
				$response = $data;
			}
			
			Encoder::set( $response );
		}
	}
?>
