-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2018 at 01:03 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ifhti_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_branch`
--

CREATE TABLE `tbl_branch` (
  `b_id` varchar(100) NOT NULL,
  `b_name` text NOT NULL,
  `b_street` text NOT NULL,
  `b_barangay` text NOT NULL,
  `b_city_mun` text NOT NULL,
  `b_province` text NOT NULL,
  `b_zip` int(4) NOT NULL,
  `b_created` datetime NOT NULL,
  `b_hid` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_branch`
--

INSERT INTO `tbl_branch` (`b_id`, `b_name`, `b_street`, `b_barangay`, `b_city_mun`, `b_province`, `b_zip`, `b_created`, `b_hid`) VALUES
('0001', 'Naga Branch', 'Sto Nino', 'Liboton', 'Naga City', 'Camarines Sur', 4417, '2018-05-10 05:19:18', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_branch_account`
--

CREATE TABLE `tbl_branch_account` (
  `ba_id` varchar(100) NOT NULL,
  `b_id` varchar(100) NOT NULL,
  `ba_clerk_pc` text NOT NULL,
  `ba_staff_pc` text NOT NULL,
  `ba_admin_pc` text NOT NULL,
  `ba_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_branch_account`
--

INSERT INTO `tbl_branch_account` (`ba_id`, `b_id`, `ba_clerk_pc`, `ba_staff_pc`, `ba_admin_pc`, `ba_created`) VALUES
('BATASRW6356522423', '0001', '{\"dm\":\"9h3c55aWGmfiKBE2SMpFVg==\",\"sp\":\"f3697908a240b6159b24448a81e16b14\",\"as\":\"3399d2e2aeee6b3c\"}', '{\"dm\":\"od01gqwKmvzWFJNEMp0v5w==\",\"sp\":\"3437c3d6ce9473dd6268f4d089b3a253\",\"as\":\"e0f91042b67f8221\",\"ml\":\"8DfrpumFkJmvCd3mC7yT\"}', '{\"dm\":\"hilPjZe87uGzRJ6aFMAHkg==\",\"sp\":\"0816813b32407cf58b6b36f342cf493e\",\"as\":\"4434419d6db948cc\"}', '2018-05-10 12:25:25');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_branch_service`
--

CREATE TABLE `tbl_branch_service` (
  `bs_id` varchar(100) NOT NULL,
  `b_id` varchar(100) NOT NULL,
  `s_id` varchar(100) NOT NULL,
  `bs_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE `tbl_news` (
  `news_id` varchar(100) NOT NULL,
  `b_id` varchar(100) NOT NULL,
  `news_title` text NOT NULL,
  `news_content` text NOT NULL,
  `news_file` text NOT NULL,
  `news_posted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_note`
--

CREATE TABLE `tbl_note` (
  `n_id` varchar(100) NOT NULL,
  `b_id` varchar(100) NOT NULL,
  `n_note` text NOT NULL,
  `n_author` varchar(100) NOT NULL,
  `n_date` date NOT NULL,
  `n_timein` time NOT NULL,
  `n_timeout` time NOT NULL,
  `n_type` varchar(100) NOT NULL,
  `n_created` datetime NOT NULL,
  `n_hid` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_note`
--

INSERT INTO `tbl_note` (`n_id`, `b_id`, `n_note`, `n_author`, `n_date`, `n_timein`, `n_timeout`, `n_type`, `n_created`, `n_hid`) VALUES
('NT-18260620180528-HTX', '0001', '<p>Hello Sir</p>', 'Francis Brazal', '2018-05-01', '14:00:00', '19:00:00', 'staff', '2018-05-28 18:26:06', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_patient`
--

CREATE TABLE `tbl_patient` (
  `p_id` varchar(100) NOT NULL,
  `p_fname` text NOT NULL,
  `p_mname` text NOT NULL,
  `p_lname` text NOT NULL,
  `p_bdate` date NOT NULL,
  `p_street` text NOT NULL,
  `p_barangay` text NOT NULL,
  `p_city_mun` text NOT NULL,
  `p_province` text NOT NULL,
  `p_contact` text NOT NULL,
  `p_religion` varchar(150) NOT NULL,
  `p_nationality` varchar(200) NOT NULL,
  `p_hid` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_patient`
--

INSERT INTO `tbl_patient` (`p_id`, `p_fname`, `p_mname`, `p_lname`, `p_bdate`, `p_street`, `p_barangay`, `p_city_mun`, `p_province`, `p_contact`, `p_religion`, `p_nationality`, `p_hid`) VALUES
('201806-0001-001', 'Gerald Jorge', 'Reyes', 'Panga', '1993-07-02', '32', 'Dayangdang', 'Naga City', 'Camarines Sur', '09978545621', 'Catholic', 'Filipino', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_patient_record`
--

CREATE TABLE `tbl_patient_record` (
  `pr_id` varchar(100) NOT NULL,
  `p_id` varchar(100) NOT NULL,
  `b_id` varchar(100) NOT NULL,
  `pr_date` date NOT NULL,
  `pr_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_patient_record`
--

INSERT INTO `tbl_patient_record` (`pr_id`, `p_id`, `b_id`, `pr_date`, `pr_created`) VALUES
('PR-04024920180629-MSU', '201806-0001-001', '0001', '2018-06-29', '2018-06-29 04:02:49');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_patient_service`
--

CREATE TABLE `tbl_patient_service` (
  `ps_id` varchar(100) NOT NULL,
  `p_id` varchar(100) NOT NULL,
  `s_id` varchar(100) NOT NULL,
  `ps_created` datetime NOT NULL,
  `ps_date` date NOT NULL,
  `with_philhealth` int(1) NOT NULL,
  `pay_no` varchar(100) DEFAULT NULL,
  `ps_hid` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_patient_service`
--

INSERT INTO `tbl_patient_service` (`ps_id`, `p_id`, `s_id`, `ps_created`, `ps_date`, `with_philhealth`, `pay_no`, `ps_hid`) VALUES
('PS-04223320180629-Y3M', '201806-0001-001', 'SRV-11555620180524-ENU', '2018-06-29 04:22:33', '2018-06-28', 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE `tbl_payment` (
  `pay_no` varchar(100) NOT NULL,
  `p_id` varchar(100) NOT NULL,
  `ps_id` varchar(100) NOT NULL,
  `pay_date` datetime NOT NULL,
  `pay_amount` decimal(11,2) NOT NULL,
  `pay_remarks` text NOT NULL,
  `pay_created` datetime NOT NULL,
  `pay_hid` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_philhealth`
--

CREATE TABLE `tbl_philhealth` (
  `ph_id` varchar(100) NOT NULL,
  `p_id` varchar(100) NOT NULL,
  `ph_no` varchar(14) NOT NULL,
  `ph_date_applied` date NOT NULL,
  `ph_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_philhealth_transmittal`
--

CREATE TABLE `tbl_philhealth_transmittal` (
  `pt_id` varchar(100) NOT NULL,
  `pt_name` text NOT NULL,
  `pt_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_report`
--

CREATE TABLE `tbl_report` (
  `r_id` varchar(100) NOT NULL,
  `r_filename` text NOT NULL,
  `r_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service`
--

CREATE TABLE `tbl_service` (
  `s_id` varchar(100) NOT NULL,
  `s_name` text NOT NULL,
  `s_created` datetime NOT NULL,
  `s_hid` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_service`
--

INSERT INTO `tbl_service` (`s_id`, `s_name`, `s_created`, `s_hid`) VALUES
('SRV-11555620180524-ENU', 'Checkup', '2018-05-24 11:55:56', 0),
('SRV-11560920180524-GPN', 'The Service', '2018-05-24 11:56:09', 0),
('SRV-11563520180524-LYF', 'Push Pull', '2018-05-24 11:56:35', 0),
('SRV-11564620180524-S2B', 'Stink Test', '2018-05-24 11:56:46', 0),
('SRV-14265920180516-ITP', 'Delivery', '2018-05-16 14:26:59', 0),
('SRV-14282320180516-6DD', 'Family Planning', '2018-05-16 14:28:23', 0),
('SRV-14301920180516-SJK', 'IMCI', '2018-05-16 14:30:19', 0),
('SRV-17174420180528-TLH', 'Heart Removal', '2018-05-28 17:17:44', 0),
('SRV-18082620180520-HE4', 'IUD Insertion', '2018-05-20 18:08:26', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service_record`
--

CREATE TABLE `tbl_service_record` (
  `sr_id` varchar(100) NOT NULL,
  `sr_date` date NOT NULL,
  `sr_name` text NOT NULL,
  `sr_status` text NOT NULL,
  `sr_created` datetime NOT NULL,
  `sr_hid` tinyint(1) NOT NULL,
  `s_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service_requirement`
--

CREATE TABLE `tbl_service_requirement` (
  `srq_id` varchar(100) NOT NULL,
  `s_id` varchar(100) NOT NULL,
  `srq_name` text NOT NULL,
  `srq_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_service_requirement`
--

INSERT INTO `tbl_service_requirement` (`srq_id`, `s_id`, `srq_name`, `srq_created`) VALUES
('SRQ-12122120180527-4JH', 'SRV-11555620180524-ENU', 'Application Form', '2018-05-27 12:12:21'),
('SRQ-12172520180527-VNP', 'SRV-11555620180524-ENU', 'Statistics', '2018-05-27 12:17:25'),
('SRQ-13000720180527-QEM', 'SRV-14265920180516-ITP', 'Delivery Form', '2018-05-27 13:00:07'),
('SRQ-15495720180527-D4E', 'SRV-11560920180524-GPN', 'Service Form', '2018-05-27 15:49:57'),
('SRQ-16054920180527-JKQ', 'SRV-11560920180524-GPN', 'Complete', '2018-05-27 16:05:49'),
('SRQ-16263420180527-FPW', 'SRV-11560920180524-GPN', 'Form #1', '2018-05-27 16:26:34'),
('SRQ-16265920180527-ERX', 'SRV-11560920180524-GPN', 'Form #2', '2018-05-27 16:26:59'),
('SRQ-16271620180527-OMT', 'SRV-11560920180524-GPN', 'Form #3', '2018-05-27 16:27:16'),
('SRQ-17175620180528-PGY', 'SRV-17174420180528-TLH', 'Heart Image', '2018-05-28 17:17:56'),
('SRQ-18225020180528-OPR', 'SRV-14282320180516-6DD', 'Application Form', '2018-05-28 18:22:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_branch`
--
ALTER TABLE `tbl_branch`
  ADD PRIMARY KEY (`b_id`);

--
-- Indexes for table `tbl_branch_account`
--
ALTER TABLE `tbl_branch_account`
  ADD PRIMARY KEY (`ba_id`);

--
-- Indexes for table `tbl_branch_service`
--
ALTER TABLE `tbl_branch_service`
  ADD PRIMARY KEY (`bs_id`);

--
-- Indexes for table `tbl_news`
--
ALTER TABLE `tbl_news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `tbl_note`
--
ALTER TABLE `tbl_note`
  ADD PRIMARY KEY (`n_id`);

--
-- Indexes for table `tbl_patient`
--
ALTER TABLE `tbl_patient`
  ADD PRIMARY KEY (`p_id`);

--
-- Indexes for table `tbl_patient_record`
--
ALTER TABLE `tbl_patient_record`
  ADD PRIMARY KEY (`pr_id`);

--
-- Indexes for table `tbl_philhealth`
--
ALTER TABLE `tbl_philhealth`
  ADD PRIMARY KEY (`ph_id`);

--
-- Indexes for table `tbl_philhealth_transmittal`
--
ALTER TABLE `tbl_philhealth_transmittal`
  ADD PRIMARY KEY (`pt_id`);

--
-- Indexes for table `tbl_report`
--
ALTER TABLE `tbl_report`
  ADD PRIMARY KEY (`r_id`);

--
-- Indexes for table `tbl_service`
--
ALTER TABLE `tbl_service`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `tbl_service_record`
--
ALTER TABLE `tbl_service_record`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `tbl_service_requirement`
--
ALTER TABLE `tbl_service_requirement`
  ADD PRIMARY KEY (`srq_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
