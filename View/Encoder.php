<?php
    
    
    header('Content-Type: application/json');

    class Encoder
    {
		public static function set($obj)
        {
			/**
				#Uncomment this line for Encrypted Response
				
				echo cryptoJsAesEncrypt( json_encode($obj, JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK) );
			**/
			if( is_array($obj) )
			{
				if(isset($obj['status']))
				{
//					http_response_code($obj['status']);
				}
			}
			self::encode( $obj );
        }
		public static function mpd()
		{
			$code = 400;
//			http_response_code ($code);
			$defined = [ 'status' => $code,
						 'message'=> 'Missing Post Data'
					   ];
			if(func_num_args() > 0)
			{
				$response = array_merge( $defined ,	func_get_arg(0) );
			}else
			{
				$response = $defined;
			}
			self::encode( $response );
			
			
		}
		public static function invalid()
		{
			$code = 403;
//			http_response_code ($code);

			if( func_num_args() > 0 )
			{
				$obj = func_get_arg(0);
				$obj = (is_array( $obj )) ? $obj : [ 'data' => $obj ];
				self::encode(array_merge(
					[ 
						'status'=> $code,
				  		'message' => 'Invalid Request'
					], $obj
				));
			}else
			{
				self::encode(
					[ 
						'status'=> $code,
				  		'message' => 'Invalid Request'
					]
				);
			}
			
		}
		public static function success($object)
		{
			$code = 201;
//			http_response_code ($code);
			self::encode(array_merge(
				['status' => $code,
				 'message'=> 'Success'
				],
				$object
			));
		}
		public static function encode( $obj )
		{
			//JSON_NUMERIC_CHECK
			echo json_encode($obj, JSON_PRETTY_PRINT);
		}
    }

?>